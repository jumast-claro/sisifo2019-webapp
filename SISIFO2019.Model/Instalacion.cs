﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace SISIFO2019.Model
{
    public class DetallesUbicacion
    {
        public int? Piso { get; set; }

        public string Comentarios { get; set; }

        public double? Latitud { get; set; }
        public double? Longitud { get; set; }
    }

    public class Instalacion
    {
        public int Id { get; set; }

        public string Cliente { get; set; }

        public string ClienteFinal { get; set; }


        public DateTime FechaInicio { get; set; }


        public DateTime FechaCompromiso { get; set; }

        // ReSharper disable once InconsistentNaming
        public int? PMAsignadoId { get; set; }

        [ForeignKey("PMAsignadoId")] public IntegranteMdD PMAsignado { get; set; }

        public bool EstaPriorizada { get; set; }

        public int? DuenioId { get; set; }

        [ForeignKey("DuenioId")] public IntegranteInstFO Duenio { get; set; }

        public List<OrdenCRM> Ordenes { get; set; }

        public Etapa? Etapa { get; set; }

        public Estado Estado { get; set; }

        public Responsable? Responsable { get; set; }


        public List<Tarea> Tareas { get; set; }

        public string Nota { get; set; }


        public int? UbicacionGeograficaId { get; set; }

        [ForeignKey("UbicacionGeograficaId")] public UbicacionGeografica UbicacionGeografica { get; set; }

        public DetallesUbicacion DetallesUbicacion { get; set; } = new DetallesUbicacion();

        public Contratista? Contratista { get; set; }

        public string UbicacionTecnica { get; set; }

        public string NombreDeProyecto { get; set; }

        public string ComentarioEstado { get; set; }

        public List<Nota> Notas { get; set; }

        public DateTime? FechaTerminada { get; set; }

        public Tecnologia Tecnologia { get; set; }

        public bool? OnTimeCrudo
        {
            get
            {
                switch (this.Estado)
                {
                    case Estado.Cancelada:
                        return this.FechaTerminada.Value <= this.FechaCompromiso;
                    case Estado.Terminada:
                        return this.FechaTerminada.Value <= this.FechaCompromiso;
                    case Estado.EnProceso:
                        return DateTime.Now <= this.FechaCompromiso;
                    case Estado.Interrumpida:
                        return DateTime.Now <= this.FechaCompromiso;
                    default:
                        throw new InvalidEnumArgumentException();
                }
            }
        }

        public int TiempoBrutoInstalacionEnDias
        {
            get
            {
                if (this.Estado == Estado.Interrumpida || this.Estado == Estado.EnProceso)
                {
                    return (DateTime.Now - this.FechaInicio).Days;
                }

                else
                {
                    return (this.FechaTerminada.Value - this.FechaInicio).Days;
                }
            }
        }

        public int TiempoEnPermisosEnDias
        {
            get { return this.Interrupciones.Where(i => i.Responsable == ResponsableInterrupcion.Permisos).Sum(i => i.DuracionEnDias); }
        }

        public int TiempoInterrumpidaEnDias
        {
            get { return this.Interrupciones.Where(i => i.Responsable != ResponsableInterrupcion.Permisos).Sum(i => i.DuracionEnDias); }
        }

        public int TiempoNetoEnDias
        {
            get
            {
                var tiempoInterrumpidaEnDias = this.Interrupciones.Sum(i => i.DuracionEnDias);
                return this.TiempoBrutoInstalacionEnDias - tiempoInterrumpidaEnDias;
            }
        }

        public List<Interrupcion> Interrupciones { get; set; }
    }
}
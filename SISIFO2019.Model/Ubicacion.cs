using System.Dynamic;

namespace SISIFO2019.Model
{
    public class Ubicacion
    {
        public int Id { get; set; }
        public int InstalacionId { get; set; }
        
        public string ProvinciaId { get; set; }
        public string ProvinciaNombre { get; set; }

        public string DepartamentoId { get; set; }
        public string DepartamentoNombre { get; set; }
        
        public string MunicipioId { get; set; }
        public string MunicipioNombre { get; set; }
        
        public string LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }
        
        public string CalleId { get; set; }
        public string CalleNombre { get; set; }
        
        public int? CalleNumero { get; set; }
        
        public int? Piso { get; set; }
        
        public double? Latitud { get; set; }
        public double? Longitud { get; set; }
        
        public string DireccionDesnormalizada { get; set; }

    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SISIFO2019.Model
{
    public class Nota
    {
        public int Id { get; set; }
        public int InstalacionId { get; set; }
        
        public string Contenido { get; set; }
        public DateTime Fecha { get; set; }
        
        public int UsuarioId { get; set; }
        [ForeignKey(("UsuarioId"))]
        public IntegranteInstFO Usuario { get; set; }
    }
}
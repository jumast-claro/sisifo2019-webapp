﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Globalization;

namespace SISIFO2019.Model
{
    public class ClienteOrden
    {
        public string RazonSocial { get; set; }
        public string CUIT { get; set; }
    }

    public class SitioInstalacionOrden
    {
        public string ProvinciaId { get; set; }

        public string ProvinciaNombre { get; set; }

        public string DepartamentoId { get; set; }

        public string DepartamentoNombre { get; set; }

        public string Direccion { get; set; }
        
        
        public int Piso { get; set; }
        
        public string Detalles { get; set; }
       
    }

    public class PMAsignadoAOrden
    {
        public int Identificador { get; set; }
        
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }

    public class CierreOrden
    {
        [DataType(DataType.Date)]
        public DateTime? FechaCierre { get; set; }
        
        public ResultadoOrden? Resultado { get; set; } 
    }


    // ReSharper disable once InconsistentNaming
    public class OrdenCRM
    {
        public int Id { get; set; }
        
        public int? InstalacionId { get; set; }

        public Instalacion Instalacion { get; set; }
        
        public PMAsignadoAOrden PmAsignado { get; set; }
        
        public string UbicacionTecnica { get; set; } 

        public CRM CRM { get; set; }

        public TipoDeOrden Tipo { get; set; }

        public ClienteOrden Cliente { get; set; }

        [Required]
        public string NumeroOrdenCRM { get; set; }

        [Required]
        public string NumeroEnlace { get; set; }

        public string NumeroOrdenSAP { get; set; }
        
        [Required]
        public SitioInstalacionOrden Ubicacion { get; set; }

        [DataType(DataType.Date)]
        public DateTime FechaInicio { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FechaCompromiso { get; set; }
      

        public CierreOrden Cierre { get; set; } = new CierreOrden();
        
        public string Comentarios { get; set; }
        
        public string TicketId { get; set; }

        public int? ResponsableInstFOId { get; set; }
        
        
        [ForeignKey("ResponsableInstFOId")]
        public IntegranteInstFO ResponsableInstFO { get; set; }
        

    }
}
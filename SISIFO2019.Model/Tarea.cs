﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Text;

namespace SISIFO2019.Model
{
    public class Tarea
    {
        public int Id { get; set; }

        public int InstalacionId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime? FechaInicio { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime? FechaFin { get; set; }

        public TipoDeTarea Tipo { get; set; }

        public Contratista Contratista { get; set; }

        public string Descripcion { get; set; }

        public EstadoDeTarea Estado { get; set; }
        
        //
        
        public bool RequiereCoordinacion { get; set; }
        public EstadoCoordinacion? EstadoCoordinacion { get; set; }
        public Responsable? ResponsableCoordinacion { get; set; }
        public string ComentarioCoordinacion { get; set; }
        
        public int DuracionEnHoras { get; set; }
        public string TecnicoResponsable { get; set; }
        
        public string ComentarioEstado { get; set; }

        public Instalacion Instalacion { get; set; }

    }
}

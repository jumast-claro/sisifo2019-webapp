namespace SISIFO2019.Model
{
    public class Municipio
    {
        public string Id { get; set; }
        
        public string ProvinciaId { get; set; }
        
        public string Nombre { get; set; }
        
        public string ProvinciaNombre { get; set; }

        public string NombreConProvincia => this.Nombre + " - " + this.ProvinciaNombre;
    }
}
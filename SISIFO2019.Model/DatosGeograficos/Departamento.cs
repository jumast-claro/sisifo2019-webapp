namespace SISIFO2019.Model.DatosGeograficos
{
    public class Departamento
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string ProvinciaId { get; set; }
        public string ProvinciaNombre { get; set; }
    }
}
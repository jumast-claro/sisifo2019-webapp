namespace SISIFO2019.Model
{
    public class LocalidadCensal
    {
        public long Id { get; set; }
        public int MunicipioId { get; set; }
        public int DepartamentoId { get; set; }
    }
}
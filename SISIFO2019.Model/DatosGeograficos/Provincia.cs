using System.Dynamic;

namespace SISIFO2019.Model
{
    public class Provincia
    {
        public string Id { get; set; }
        public string Nombre { get; set; }

    }
}
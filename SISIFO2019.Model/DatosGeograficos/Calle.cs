using System;
using System.Dynamic;

namespace SISIFO2019.Model
{
    public class Calle
    {
        public string Id { get; set; }

        public string ProvinciaId { get; set; }
        public int DepartamentoId { get; set; }
        public string Nombre { get; set; }
        public string DepartamentoNombre { get; set; }
        public string ProvinciaNombre { get; set; }

    }
}
namespace SISIFO2019.Model.DatosGeograficos
{
    public class Localidad
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string DepartamentoId { get; set; }
        public string DepartamentoNombre { get; set; }
    }
}
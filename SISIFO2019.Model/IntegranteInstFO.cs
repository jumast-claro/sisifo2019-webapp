using System.Dynamic;

namespace SISIFO2019.Model
{
    // ReSharper disable once InconsistentNaming
    public class IntegranteInstFO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NombreCorto { get; set; }
        public string NombreCompleto => NombreCorto + " " + Apellido;
    }
}
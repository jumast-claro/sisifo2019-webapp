﻿using System;

namespace SISIFO2019.Model
{
    public class Interrupcion
    {
        public int Id { get; set; }
        public int InstalacionId { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public ResponsableInterrupcion Responsable { get; set; }
        public string Comentarios { get; set; }

        public int DuracionEnDias
        {
            get
            {
                var fecha = FechaFin ?? DateTime.Now;
                return (fecha - FechaInicio).Days;
            }
        }
        
        public Etapa EtapaInstalacion { get; set; }
    }
}
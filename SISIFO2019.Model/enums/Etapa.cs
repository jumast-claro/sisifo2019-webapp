﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace SISIFO2019.Model
{
    public enum Etapa
    {
        Analisis = 1,
        DefinicionDeProyecto = 2,
        Relevamiento = 3,
        Sondeos = 4,
        Permisos = 5,
        ObraCivil = 6,
        Tendido = 7,
        Empalmes = 8,
        Insercion = 9,
        StartUp = 10,
        ABM = 11,
        Mantenimiento = 12,
        Garantia = 13,
        Terminada = 14,
    }
}
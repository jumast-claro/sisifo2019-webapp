﻿using System.ComponentModel.DataAnnotations;

namespace SISIFO2019.Model
{
    public enum TipoDeTarea
    {
        Relevamiento = 1,
        Sondeo,

        [Display(Name = "Obra civil")]
        ObraCivil,
        Tendido,
        Empalme,
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace SISIFO2019.Model
{
    public enum Estado
    {
        EnProceso = 1,
        Interrumpida = 2,
        Terminada = 3,
        Cancelada = 4
    }
}
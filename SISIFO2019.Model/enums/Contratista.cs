﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace SISIFO2019.Model
{
    public enum Contratista
    {
        Aucatek = 1,
        Ibercom,

        [Display(Name = "Gómez")]
        Gomez,

        Netacom,
    }
}
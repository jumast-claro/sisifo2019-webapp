﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace SISIFO2019.Model
{
    public enum Responsable
    {
        Comercial = 1,
        Consultoria = 2,
        MesaDeDespacho = 3,
        PM = 4,
        GestionDeIngresos = 5,
        Ingenieria = 6,
        AdministracionFO = 7,
        Permisos = 8,
        InstalacionesFO = 9,
        InsercionesAMBA = 10,
        StartUp = 11,
        Mantenimiento = 12,
        Contratista = 13,
        Cliente = 14,
    }
}
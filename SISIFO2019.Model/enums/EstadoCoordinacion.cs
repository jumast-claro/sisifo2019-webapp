﻿namespace SISIFO2019.Model
{
    public enum EstadoCoordinacion
    {
        Pendiente = 1,
        EnProceso = 2,
        Interrumpida = 3,
        Terminada = 4,
        Cancelada = 5,
    }
}
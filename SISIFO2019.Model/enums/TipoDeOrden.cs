﻿using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace SISIFO2019.Model
{
    public enum TipoDeOrden
    {
        Alta = 1,
        [Display(Name = "Optimización")]
        Optimizacion = 2,
        Mudanza = 3,
        OTI = 4,
        MT = 5
    }
}
﻿// ReSharper disable once CheckNamespace

using System.ComponentModel.DataAnnotations;

namespace SISIFO2019.Model
{
    public enum CRM
    {
        [Display(Name = "VTV")]
        Vantive = 1,

        [Display(Name = "WFC")]
        WorkflowClient = 2,
    }
}
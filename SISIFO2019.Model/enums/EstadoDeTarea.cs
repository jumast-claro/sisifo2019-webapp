﻿using System.ComponentModel.DataAnnotations;

namespace SISIFO2019.Model
{
    public enum EstadoDeTarea
    {
        Pendiente = 1,
        EnEjecucion = 2,
        EjecutadaParcialmente = 3,
        Terminada = 4,
        Cancelada = 5,
    }
}
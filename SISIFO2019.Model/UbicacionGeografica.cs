using System.Collections.Generic;

namespace SISIFO2019.Model
{
    public class UbicacionGeografica
    {
        public int Id { get; set; }
        
        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public string ProvinciaId { get; set; }
        public string ProvinciaNombre { get; set; }

        public string DepartamentoId { get; set; }
        public string DepartamentoNombre { get; set; }

        public string MunicipioId { get; set; }
        public string MunicipioNombre { get; set; }
        
        public string Direccion { get; set; }
        
        public string CalleId { get; set; }
        public string CalleNombre { get; set; }
        
        public string Altura { get; set; }

        public string LocalidadId { get; set; }
        public string LocalidadNombre { get; set; }

        public string CodigoPostal { get; set; }

        public string Comentarios { get; set; }
        
        public List<Instalacion> Instalaciones { get; set; }
        
        public string IdentificadorCatastral { get; set; }
        
        public string Nombre { get; set; }
        
    }
}
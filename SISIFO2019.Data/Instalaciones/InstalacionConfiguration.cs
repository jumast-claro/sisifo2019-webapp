﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class InstalacionConfiguration : IEntityTypeConfiguration<Instalacion>
    {
        public void Configure(EntityTypeBuilder<Instalacion> builder)
        {
            builder.ToTable("instalaciones");
            builder.Property(i => i.Id).HasColumnName("id");
            builder.Property(i => i.Cliente).HasColumnName("cliente");
            builder.Property(i => i.ClienteFinal).HasColumnName("cliente_final");
            builder.Property(i => i.FechaInicio).HasColumnName("inicio_fecha");
            builder.Property(i => i.FechaCompromiso).HasColumnName("compromiso_fecha");
            builder.Property(i => i.PMAsignadoId).HasColumnName("pm_id");
            builder.Property(i => i.EstaPriorizada).HasColumnName("esta_priorizada");
            builder.Property(i => i.DuenioId).HasColumnName("hipervisor_id");
            builder.Property(i => i.Etapa).HasColumnName("etapa");
            builder.Property(i => i.Estado).HasColumnName("estado");
            builder.Property(i => i.Responsable).HasColumnName("responsable");
            builder.Property(i => i.Nota).HasColumnName("nota");
            builder.Property(i => i.UbicacionGeograficaId).HasColumnName("sitio_id");

            builder.Property(o => o.EstaPriorizada).HasConversion<int>();

            builder.OwnsOne(i => i.DetallesUbicacion, du =>
            {
                du.Property(x => x.Piso).HasColumnName("ubicacion_piso");
                du.Property(x => x.Comentarios).HasColumnName("ubicacion_comentarios");
                du.Property(x => x.Latitud).HasColumnName("latitud");
                du.Property(x => x.Longitud).HasColumnName("longitud");
            });

            builder.Property(i => i.NombreDeProyecto).HasColumnName("proyecto_nombre");
            builder.Property(i => i.UbicacionTecnica).HasColumnName("ubicacion_tecnica");
            builder.Property(i => i.Contratista).HasColumnName("contratista");
            builder.Property(i => i.ComentarioEstado).HasColumnName("estado_comentario");

            builder.Property(i => i.FechaTerminada).HasColumnName("terminada_fecha");
            builder.Property(i => i.Tecnologia).HasColumnName("tecnologia");
            builder.Property(i => i.Tecnologia).HasConversion<string>();
        }
    }
}
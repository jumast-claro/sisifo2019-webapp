using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class IntegranteInstFOConfiguration : IEntityTypeConfiguration<IntegranteInstFO>
    {
        public void Configure(EntityTypeBuilder<IntegranteInstFO> builder)
        {
            builder.ToTable("instfo_integrantes");
            builder.Property(i => i.Id).HasColumnName("id");
            builder.Property(i => i.Nombre).HasColumnName("nombre");
            builder.Property(i => i.Apellido).HasColumnName("apellido");
            builder.Property(i => i.NombreCorto).HasColumnName("nombre_corto");
            
        }   
        
    }
    
    public class IntegranteMdDConfiguration : IEntityTypeConfiguration<IntegranteMdD>
    {
        public void Configure(EntityTypeBuilder<IntegranteMdD> builder)
        {
            builder.ToTable("mdd_integrantes");
            builder.Property(i => i.Id).HasColumnName("id");
            builder.Property(i => i.Nombre).HasColumnName("nombre");
            builder.Property(i => i.Apellido).HasColumnName("apellido");
            
        }   
        
    }
}
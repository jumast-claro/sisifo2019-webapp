﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class TareaConfiguration : IEntityTypeConfiguration<Tarea>
    {
        public void Configure(EntityTypeBuilder<Tarea> builder)
        {
            builder.ToTable("tareas");
            builder.Property(t => t.Id).HasColumnName("id");
            builder.Property(t => t.InstalacionId).HasColumnName("instalacion_id");
            builder.Property(t => t.FechaInicio).HasColumnName("inicio_fecha");
            builder.Property(t => t.FechaFin).HasColumnName("fin_fecha");

            builder.Property(t => t.Tipo).HasColumnName("tipo").HasConversion<string>();
            builder.Property(t => t.Estado).HasColumnName("estado").HasConversion<string>();
            builder.Property(t => t.Contratista).HasColumnName("contratista").HasConversion<string>();
            builder.Property(t => t.Descripcion).HasColumnName("descripcion");

            builder.Property(t => t.RequiereCoordinacion)
                .HasColumnName("requiere_coordinacion")
                .HasConversion<int>();
                
            builder.Property(t => t.EstadoCoordinacion)
                .HasColumnName("coordinacion_estado")
                .HasConversion<string>();
            builder.Property(t => t.ResponsableCoordinacion)
                .HasColumnName("coordinacion_responsable")
                .HasConversion<string>();
            builder.Property(t => t.ComentarioCoordinacion).HasColumnName("coordinacion_comentario");

            builder.Property(t => t.DuracionEnHoras).HasColumnName("duracion_hs");
            builder.Property(t => t.TecnicoResponsable).HasColumnName("tecnico_responsable");

            builder.Property(t => t.ComentarioEstado).HasColumnName("estado_comentario");
        }
    }
}
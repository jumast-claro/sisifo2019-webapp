using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class UbicacionGeograficaConfguration : IEntityTypeConfiguration<UbicacionGeografica>
    {
        public void Configure(EntityTypeBuilder<UbicacionGeografica> builder)
        {
            builder.ToTable("sitios");
            builder.Property(u => u.Id).HasColumnName("id");
            builder.Property(u => u.Latitud).HasColumnName("latitud").IsRequired();
            builder.Property(u => u.Longitud).HasColumnName("longitud").IsRequired();
            builder.Property(u => u.ProvinciaId).HasColumnName("provincia_id").IsRequired();
            builder.Property(u => u.ProvinciaNombre).HasColumnName("provincia_nombre").IsRequired();
            builder.Property(u => u.DepartamentoId).HasColumnName("departamento_id").IsRequired();
            builder.Property(u => u.DepartamentoNombre).HasColumnName("departamento_nombre").IsRequired();
            builder.Property(u => u.MunicipioId).HasColumnName("municipio_id");
            builder.Property(u => u.MunicipioNombre).HasColumnName("municipio_nombre");
            builder.Property(u => u.Direccion).HasColumnName("direccion").IsRequired();
            builder.Property(u => u.CalleId).HasColumnName("calle_id");
            builder.Property(u => u.CalleNombre).HasColumnName("calle_nombre");
            builder.Property(u => u.Altura).HasColumnName("altura");
            builder.Property(u => u.LocalidadId).HasColumnName("localidad_id");
            builder.Property(u => u.LocalidadNombre).HasColumnName("localidad_nombre");
            builder.Property(u => u.CodigoPostal).HasColumnName("codigo_postal");
            builder.Property(u => u.Comentarios).HasColumnName("comentarios");
            
            builder.Property(u => u.IdentificadorCatastral).HasColumnName("identificador_catastral");
            builder.Property(u => u.Nombre).HasColumnName("nombre");
            
            
        }
    }
}
﻿using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Model;
using SISIFO2019.Model.DatosGeograficos;

namespace SISIFO2019.Data
{
    public class InstalacionesContext : DbContext
    {
        public InstalacionesContext(DbContextOptions<InstalacionesContext> options) : base(options)
        {

        }

        public DbSet<Instalacion> Instalaciones { get; set; }

        public DbSet<OrdenCRM> Ordenes { get; set; }

        public DbSet<Tarea> Tareas { get; set; }
        
        public DbSet<IntegranteMdD> PMs { get; set; }
        
        public DbSet<IntegranteInstFO> IntegrantesInstalacionesFO { get; set; } 
        

        public DbSet<UbicacionGeografica> UbicacionesGeograficas { get; set; }
        
        public DbSet<Nota> Notas { get; set; }
        
        public DbSet<Interrupcion> Interrupciones { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new InstalacionConfiguration());
            modelBuilder.ApplyConfiguration(new OrdenCRMConfiguration());
            modelBuilder.ApplyConfiguration(new UbicacionGeograficaConfguration());
            modelBuilder.ApplyConfiguration(new TareaConfiguration());
            modelBuilder.ApplyConfiguration(new IntegranteInstFOConfiguration());
            modelBuilder.ApplyConfiguration(new IntegranteMdDConfiguration());
            modelBuilder.ApplyConfiguration(new NotaConfiguration());
            modelBuilder.ApplyConfiguration(new InterrupcionConfiguration());
        }
    }
}

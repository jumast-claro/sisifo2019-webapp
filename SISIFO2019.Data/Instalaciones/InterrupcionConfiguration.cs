﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
        public class InterrupcionConfiguration : IEntityTypeConfiguration<Interrupcion>
        {
            public void Configure(EntityTypeBuilder<Interrupcion> builder)
            {
                builder.ToTable("interrupciones");
                builder.Property(i => i.Id).HasColumnName("id");
                builder.Property(i => i.InstalacionId).HasColumnName("instalacion_id");
                builder.Property(i => i.FechaInicio).HasColumnName("inicio_fecha");
                builder.Property(i => i.FechaFin).HasColumnName("fin_fecha");
                builder.Property(i => i.Responsable).HasColumnName("responsable").HasConversion<string>();
                builder.Property(i => i.Comentarios).HasColumnName("comentarios");
                builder.Property(i => i.EtapaInstalacion).HasColumnName("instalacion_etapa").HasConversion<string>();
            }
        }
}
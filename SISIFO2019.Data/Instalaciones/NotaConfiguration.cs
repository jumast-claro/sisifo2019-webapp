﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class NotaConfiguration : IEntityTypeConfiguration<Nota>
    {
        public void Configure(EntityTypeBuilder<Nota> builder)
        {
            builder.ToTable("notas");
            builder.Property(n => n.Id).HasColumnName("id");
            builder.Property(n => n.InstalacionId).HasColumnName("instalacion_id");
            builder.Property(n => n.Contenido).HasColumnName("contenido");
            builder.Property(n => n.Fecha).HasColumnName("fecha");
            builder.Property(n => n.UsuarioId).HasColumnName("usuario_id");

            builder.Property(n => n.Contenido).IsRequired();

        }
    }
}
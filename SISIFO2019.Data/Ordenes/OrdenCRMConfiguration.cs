﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class OrdenCRMConfiguration : IEntityTypeConfiguration<OrdenCRM>
    {
        public void Configure(EntityTypeBuilder<OrdenCRM> builder)
        {
            builder.ToTable("ordenes");
            builder.HasKey("Id");
            builder.Property(o => o.Id).HasColumnName("id");
            builder.Property(o => o.InstalacionId).HasColumnName("instalacion_id");
            builder.Property(o => o.UbicacionTecnica).HasColumnName("ubicacion_tecnica");
            
            builder.Property(o => o.NumeroOrdenCRM).HasColumnName("num_orden_crm");
            builder.Property(o => o.NumeroOrdenSAP).HasColumnName("num_orden_sap");
            builder.Property(o => o.NumeroEnlace).HasColumnName("num_enlace");
            
            builder.Property(o => o.CRM).HasColumnName("crm_id");
            builder.Property(o => o.CRM).HasConversion<string>();
            
            builder.Property(o => o.Tipo).HasColumnName("tipo_orden_id");
            builder.Property(o => o.Tipo).HasConversion<string>();

            builder.OwnsOne(o => o.Cliente, cliente =>
            {
                cliente.Property(c => c.RazonSocial).HasColumnName("cliente_razon_social");
                cliente.Property(c => c.CUIT).HasColumnName("cliente_cuit");
            });

            builder.OwnsOne(o => o.Ubicacion, ubicacion =>
            {
                ubicacion.Property(u => u.ProvinciaId).HasColumnName("provincia_id");
                ubicacion.Property(u => u.ProvinciaNombre).HasColumnName("provincia_nombre");
                ubicacion.Property(u => u.DepartamentoId).HasColumnName("departamento_id");
                ubicacion.Property(u => u.DepartamentoNombre).HasColumnName("departamento_nombre");
                ubicacion.Property(u => u.Direccion).HasColumnName("direccion");
                ubicacion.Property(u => u.Piso).HasColumnName("ubicacion_piso");
                ubicacion.Property(u => u.Detalles).HasColumnName("ubicacion_detalles");
            });

            builder.OwnsOne(o => o.PmAsignado, pm =>
            {
                pm.Property(p => p.Identificador).HasColumnName("pm_id");
                pm.Property(p => p.Nombre).HasColumnName("pm_nombre");
                pm.Property(p => p.Apellido).HasColumnName("pm_apellido");
            });

            builder.OwnsOne(o => o.Cierre, cierre =>
            {
                cierre.Property(c => c.FechaCierre).HasColumnName("cierre_fecha");
                cierre.Property(c => c.Resultado).HasColumnName("cierre_resultado");
            });

            builder.Property(o => o.FechaInicio).HasColumnName("inicio_fecha");
            builder.Property(o => o.FechaCompromiso).HasColumnName("compromiso_fecha");

            builder.Property(o => o.Comentarios).HasColumnName("comentarios");

            builder.Property(o => o.TicketId).HasColumnName("ticket_id");
            builder.Property(o => o.ResponsableInstFOId).HasColumnName("instfo_responsable_id");

        }
    }
}
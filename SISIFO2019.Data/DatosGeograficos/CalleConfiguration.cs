using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class CalleConfiguration : IEntityTypeConfiguration<Calle>
    {
        public void Configure(EntityTypeBuilder<Calle> builder)
        {
            builder.ToTable("calles");


            builder.Property(c => c.Id).HasColumnName("id");
//            builder.Property(c => c.ProvinciaId).HasColumnName("provincia_id");
//            builder.Property(c => c.DepartamentoId).HasColumnName("departamento_id");
            builder.Property(c => c.Nombre).HasColumnName("nombre");
//            builder.Property(c => c.Categoria).HasColumnName("categoria");
//            builder.Property(c => c.DepartamentoNombre).HasColumnName("departamento_nombre");
//            builder.Property(c => c.ProvinciaNombre).HasColumnName("provincia_nombre");

//            builder.Property(c => c.AlturaFinDerecha).HasColumnName("altura_fin_derecha");
//            builder.Property(c => c.AlturaFinIzquierda).HasColumnName("altura_fin_izquierda");
//            builder.Property(c => c.AlturaInicioDerecha).HasColumnName("altura_inicio_derecha");
//            builder.Property(c => c.AlturaInicioIzquierda).HasColumnName("altura_inicio_izquierda");
        }
    }
}
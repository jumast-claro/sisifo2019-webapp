using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;

namespace SISIFO2019.Data
{
    public class ProvinciaConfiguration : IEntityTypeConfiguration<Provincia>
    {
        public void Configure(EntityTypeBuilder<Provincia> builder)
        {
            builder.ToTable("provincias");
        }
    }
    
    public class LocalidadCensalConfiguration : IEntityTypeConfiguration<LocalidadCensal>
    {
        public void Configure(EntityTypeBuilder<LocalidadCensal> builder)
        {
            builder.ToTable("localidades-censales");
            builder.Property(lc => lc.Id).HasColumnName("id");
            builder.Property(lc => lc.MunicipioId).HasColumnName("municipio_id");
            builder.Property(lc => lc.DepartamentoId).HasColumnName("departamento_id");
        }
    }
}
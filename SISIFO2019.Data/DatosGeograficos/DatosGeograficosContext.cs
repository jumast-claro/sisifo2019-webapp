using System.Dynamic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;
using SISIFO2019.Model.DatosGeograficos;

namespace SISIFO2019.Data
{
    public class QueryProvinciasConfiguration : IQueryTypeConfiguration<Provincia>
    {
        public void Configure(QueryTypeBuilder<Provincia> queryBuilder)
        {
            queryBuilder.ToView("provincias");
            queryBuilder.Property(p => p.Id).HasColumnName("id");
            queryBuilder.Property(p => p.Nombre).HasColumnName("nombre");
        }
    }

    public class QueryDepartamentosConfiguration : IQueryTypeConfiguration<Departamento>
    {
        public void Configure(QueryTypeBuilder<Departamento> queryBuilder)
        {
            queryBuilder.ToView("departamentos");
            queryBuilder.Property(d => d.Id).HasColumnName("id");
            queryBuilder.Property(d => d.Nombre).HasColumnName("nombre");
            queryBuilder.Property(d => d.ProvinciaId).HasColumnName("provincia_id");
            queryBuilder.Property(d => d.ProvinciaNombre).HasColumnName("provincia_nombre");
        }
    }

    public class QueryMunicipiosConfiguration : IQueryTypeConfiguration<Municipio>
    {
        public void Configure(QueryTypeBuilder<Municipio> queryBuilder)
        {
            queryBuilder.ToView("municipios");
            queryBuilder.Property(m => m.Id).HasColumnName("id");
            queryBuilder.Property(m => m.Nombre).HasColumnName("nombre");
            queryBuilder.Property(m => m.ProvinciaId).HasColumnName("provincia_id");
            queryBuilder.Property(m => m.ProvinciaNombre).HasColumnName("provincia_nombre");
        }
    }

    public class QueryLocalidadesConfiguration : IQueryTypeConfiguration<Localidad>
    {
        public void Configure(QueryTypeBuilder<Localidad> queryBuilder)
        {
            queryBuilder.ToView("localidades");
            queryBuilder.Property(l => l.Id).HasColumnName("id");
            queryBuilder.Property(l => l.Nombre).HasColumnName("nombre");
            queryBuilder.Property(l => l.DepartamentoId).HasColumnName("departamento_id");
            queryBuilder.Property(l => l.DepartamentoNombre).HasColumnName("departamento_nombre");
        }
    }

    public class QueryCallesConfiguration : IQueryTypeConfiguration<Calle>
    {
        public void Configure(QueryTypeBuilder<Calle> queryBuilder)
        {
            queryBuilder.ToView("calles");
            queryBuilder.Property(c => c.Id).HasColumnName("id");
            queryBuilder.Property(c => c.Nombre).HasColumnName("nombre");
            queryBuilder.Property(c => c.ProvinciaId).HasColumnName("provincia_id");
            queryBuilder.Property(c => c.DepartamentoId).HasColumnName("departamento_id");
            queryBuilder.Property(c => c.ProvinciaNombre).HasColumnName("provincia_nombre");
            queryBuilder.Property(c => c.DepartamentoNombre).HasColumnName("departamento_nombre");
        }
    }

    public class DatosGeograficosContext : DbContext
    {
        public DatosGeograficosContext(DbContextOptions<DatosGeograficosContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new QueryProvinciasConfiguration());
            modelBuilder.ApplyConfiguration(new QueryDepartamentosConfiguration());
            modelBuilder.ApplyConfiguration(new QueryMunicipiosConfiguration());
            modelBuilder.ApplyConfiguration(new QueryLocalidadesConfiguration());
            modelBuilder.ApplyConfiguration(new QueryCallesConfiguration());
        }

        public DbQuery<Provincia> Provincias { get; set; }

        public DbQuery<Departamento> Departamentos { get; set; }

        public DbQuery<Municipio> Municipios { get; set; }

        public DbQuery<Localidad> Localidades { get; set; }

        public DbQuery<Calle> Calles { get; set; }
    }
}
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SISIFO2019.Model;
using SISIFO2019.Model.DatosGeograficos;

namespace SISIFO2019.Data
{
    public class MunicipioConfiguration : IEntityTypeConfiguration<Municipio>
    {
        public void Configure(EntityTypeBuilder<Municipio> builder)
        {
            builder.ToTable("municipios");
//            builder.Property(d => d.ProvinciaId).HasColumnName("provincia_id");
//            builder.Property(d => d.NombreProvincia).HasColumnName("provincia_nombre");
        }
    }

    public class DepartamentoConfiguration : IEntityTypeConfiguration<Departamento>
    {
        public void Configure(EntityTypeBuilder<Departamento> builder)
        {
            builder.ToTable("departamentos");
            builder.Property(m => m.Id).HasColumnName("id");
            builder.Property(m => m.Nombre).HasColumnName("nombre");
//            builder.Property(d => d.ProvinciaId).HasColumnName("provincia_id");
        }
    }

    public class LocalidadConfiguration : IEntityTypeConfiguration<Localidad>
    {
        public void Configure(EntityTypeBuilder<Localidad> builder)
        {
            builder.ToTable("localidades");
            builder.Property(m => m.Id).HasColumnName("id");
            builder.Property(m => m.Nombre).HasColumnName("nombre");
//            builder.Property(d => d.DepartamentoId).HasColumnName("departamento_id");
        }
    }
}
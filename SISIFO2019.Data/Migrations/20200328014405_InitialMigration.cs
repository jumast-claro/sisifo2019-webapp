﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "instfo_integrantes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    nombre = table.Column<string>(nullable: true),
                    apellido = table.Column<string>(nullable: true),
                    nombre_corto = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_instfo_integrantes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "mdd_integrantes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    nombre = table.Column<string>(nullable: true),
                    apellido = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mdd_integrantes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sitios",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    latitud = table.Column<double>(nullable: false),
                    longitud = table.Column<double>(nullable: false),
                    provincia_id = table.Column<string>(nullable: false),
                    provincia_nombre = table.Column<string>(nullable: false),
                    departamento_id = table.Column<string>(nullable: false),
                    departamento_nombre = table.Column<string>(nullable: false),
                    municipio_id = table.Column<string>(nullable: true),
                    municipio_nombre = table.Column<string>(nullable: true),
                    direccion = table.Column<string>(nullable: false),
                    calle_id = table.Column<string>(nullable: true),
                    calle_nombre = table.Column<string>(nullable: true),
                    altura = table.Column<string>(nullable: true),
                    localidad_id = table.Column<string>(nullable: true),
                    localidad_nombre = table.Column<string>(nullable: true),
                    codigo_postal = table.Column<string>(nullable: true),
                    comentarios = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sitios", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "instalaciones",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    cliente = table.Column<string>(nullable: true),
                    cliente_final = table.Column<string>(nullable: true),
                    inicio_fecha = table.Column<DateTime>(nullable: false),
                    compromiso_fecha = table.Column<DateTime>(nullable: false),
                    pm_id = table.Column<int>(nullable: true),
                    esta_priorizada = table.Column<int>(nullable: false),
                    hipervisor_id = table.Column<int>(nullable: true),
                    etapa = table.Column<int>(nullable: false),
                    estado = table.Column<int>(nullable: false),
                    responsable = table.Column<int>(nullable: false),
                    nota = table.Column<string>(nullable: true),
                    sitio_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_instalaciones", x => x.id);
                    table.ForeignKey(
                        name: "FK_instalaciones_instfo_integrantes_hipervisor_id",
                        column: x => x.hipervisor_id,
                        principalTable: "instfo_integrantes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_instalaciones_mdd_integrantes_pm_id",
                        column: x => x.pm_id,
                        principalTable: "mdd_integrantes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_instalaciones_sitios_sitio_id",
                        column: x => x.sitio_id,
                        principalTable: "sitios",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ordenes",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    instalacion_id = table.Column<int>(nullable: true),
                    pm_id = table.Column<int>(nullable: false),
                    pm_nombre = table.Column<string>(nullable: true),
                    pm_apellido = table.Column<string>(nullable: true),
                    ubicacion_tecnica = table.Column<string>(nullable: true),
                    crm_id = table.Column<string>(nullable: false),
                    tipo_orden_id = table.Column<string>(nullable: false),
                    cliente_razon_social = table.Column<string>(nullable: true),
                    cliente_cuit = table.Column<string>(nullable: true),
                    num_orden_crm = table.Column<string>(nullable: false),
                    num_enlace = table.Column<string>(nullable: false),
                    num_orden_sap = table.Column<string>(nullable: true),
                    provincia_id = table.Column<string>(nullable: true),
                    provincia_nombre = table.Column<string>(nullable: true),
                    departamento_id = table.Column<string>(nullable: true),
                    departamento_nombre = table.Column<string>(nullable: true),
                    direccion = table.Column<string>(nullable: true),
                    ubicacion_piso = table.Column<int>(nullable: false),
                    ubicacion_detalles = table.Column<string>(nullable: true),
                    inicio_fecha = table.Column<DateTime>(nullable: false),
                    compromiso_fecha = table.Column<DateTime>(nullable: true),
                    cierre_fecha = table.Column<DateTime>(nullable: false),
                    cierre_resultado = table.Column<int>(nullable: false),
                    comentarios = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ordenes", x => x.id);
                    table.ForeignKey(
                        name: "FK_ordenes_instalaciones_instalacion_id",
                        column: x => x.instalacion_id,
                        principalTable: "instalaciones",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tareas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    instalacion_id = table.Column<int>(nullable: false),
                    inicio_fecha = table.Column<DateTime>(nullable: true),
                    fin_fecha = table.Column<DateTime>(nullable: true),
                    tipo = table.Column<int>(nullable: false),
                    contratista = table.Column<int>(nullable: false),
                    descripcion = table.Column<string>(nullable: true),
                    estado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tareas", x => x.id);
                    table.ForeignKey(
                        name: "FK_tareas_instalaciones_instalacion_id",
                        column: x => x.instalacion_id,
                        principalTable: "instalaciones",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_instalaciones_hipervisor_id",
                table: "instalaciones",
                column: "hipervisor_id");

            migrationBuilder.CreateIndex(
                name: "IX_instalaciones_pm_id",
                table: "instalaciones",
                column: "pm_id");

            migrationBuilder.CreateIndex(
                name: "IX_instalaciones_sitio_id",
                table: "instalaciones",
                column: "sitio_id");

            migrationBuilder.CreateIndex(
                name: "IX_ordenes_instalacion_id",
                table: "ordenes",
                column: "instalacion_id");

            migrationBuilder.CreateIndex(
                name: "IX_tareas_instalacion_id",
                table: "tareas",
                column: "instalacion_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ordenes");

            migrationBuilder.DropTable(
                name: "tareas");

            migrationBuilder.DropTable(
                name: "instalaciones");

            migrationBuilder.DropTable(
                name: "instfo_integrantes");

            migrationBuilder.DropTable(
                name: "mdd_integrantes");

            migrationBuilder.DropTable(
                name: "sitios");
        }
    }
}

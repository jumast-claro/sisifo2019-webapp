﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarCamposAInstalacion07042020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.AlterColumn<string>(
                name: "contenido",
                table: "notas",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);*/

            migrationBuilder.AddColumn<double>(
                name: "latitud",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "longitud",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "terminada_fecha",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tecnologia",
                table: "instalaciones",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "latitud",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "longitud",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "terminada_fecha",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "tecnologia",
                table: "instalaciones");

            /*migrationBuilder.AlterColumn<string>(
                name: "contenido",
                table: "notas",
                nullable: true,
                oldClrType: typeof(string));*/
        }
    }
}

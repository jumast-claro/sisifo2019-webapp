﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class HacerCamposNulos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.RenameColumn(
//                name: "Nombre",
//                table: "sitios",
//                newName: "nombre");

//            migrationBuilder.RenameColumn(
//                name: "IdentificadorCatastral",
//                table: "sitios",
//                newName: "identificador_catastral");

            migrationBuilder.AlterColumn<int>(
                name: "cierre_resultado",
                table: "ordenes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "cierre_fecha",
                table: "ordenes",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.RenameColumn(
//                name: "nombre",
//                table: "sitios",
//                newName: "Nombre");

//            migrationBuilder.RenameColumn(
//                name: "identificador_catastral",
//                table: "sitios",
//                newName: "IdentificadorCatastral");

            migrationBuilder.AlterColumn<int>(
                name: "cierre_resultado",
                table: "ordenes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "cierre_fecha",
                table: "ordenes",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}

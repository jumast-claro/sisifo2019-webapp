﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarCampos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "identificador_catastral",
                table: "sitios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "nombre",
                table: "sitios",
                nullable: true);

//            migrationBuilder.AlterColumn<int>(
//                name: "id",
//                table: "instalaciones",
//                nullable: false,
//                oldClrType: typeof(int))
//                .OldAnnotation("MySQL:AutoIncrement", true);

            migrationBuilder.AddColumn<string>(
                name: "ubicacion_comentarios",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ubicacion_piso",
                table: "instalaciones",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "identificador_catastral",
                table: "sitios");

            migrationBuilder.DropColumn(
                name: "nombre",
                table: "sitios");

            migrationBuilder.DropColumn(
                name: "ubicacion_comentarios",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "ubicacion_piso",
                table: "instalaciones");

//            migrationBuilder.AlterColumn<int>(
//                name: "id",
//                table: "instalaciones",
//                nullable: false,
//                oldClrType: typeof(int))
//                .Annotation("MySQL:AutoIncrement", true);
        }
    }
}

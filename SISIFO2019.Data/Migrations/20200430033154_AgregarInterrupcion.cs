﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarInterrupcion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<int>(
            //     name: "requiere_coordinacion",
            //     table: "tareas",
            //     nullable: false,
            //     oldClrType: typeof(short));

            migrationBuilder.CreateTable(
                name: "interrupciones",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    instalacion_id = table.Column<int>(nullable: false),
                    inicio_fecha = table.Column<DateTime>(nullable: false),
                    fin_fecha = table.Column<DateTime>(nullable: true),
                    responsable = table.Column<string>(nullable: false),
                    comentarios = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_interrupciones", x => x.id);
                    table.ForeignKey(
                        name: "FK_interrupciones_instalaciones_instalacion_id",
                        column: x => x.instalacion_id,
                        principalTable: "instalaciones",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_interrupciones_instalacion_id",
                table: "interrupciones",
                column: "instalacion_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "interrupciones");

            // migrationBuilder.AlterColumn<short>(
            //     name: "requiere_coordinacion",
            //     table: "tareas",
            //     nullable: false,
            //     oldClrType: typeof(int));
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarNotasAInstalaciones : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "notas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    instalacion_id = table.Column<int>(nullable: false),
                    contenido = table.Column<string>(nullable: false),
                    fecha = table.Column<DateTime>(nullable: false),
                    usuario_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notas", x => x.id);
                    table.ForeignKey(
                        name: "FK_notas_instalaciones_instalacion_id",
                        column: x => x.instalacion_id,
                        principalTable: "instalaciones",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_notas_instfo_integrantes_usuario_id",
                        column: x => x.usuario_id,
                        principalTable: "instfo_integrantes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_notas_instalacion_id",
                table: "notas",
                column: "instalacion_id");

            migrationBuilder.CreateIndex(
                name: "IX_notas_usuario_id",
                table: "notas",
                column: "usuario_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "notas");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarCamposAInstalacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "estado_comentario",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "contratista",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "proyecto_nombre",
                table: "instalaciones",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ubicacion_tecnica",
                table: "instalaciones",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "estado_comentario",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "contratista",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "proyecto_nombre",
                table: "instalaciones");

            migrationBuilder.DropColumn(
                name: "ubicacion_tecnica",
                table: "instalaciones");
        }
    }
}

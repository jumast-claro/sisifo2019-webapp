﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarCamposATareas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.AlterColumn<string>(
                name: "tipo",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "estado",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "contratista",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(int));*/

            migrationBuilder.AddColumn<string>(
                name: "coordinacion_comentario",
                table: "tareas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "estado_comentario",
                table: "tareas",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "duracion_hs",
                table: "tareas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "coordinacion_estado",
                table: "tareas",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "requiere_coordinacion",
                table: "tareas",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<string>(
                name: "coordinacion_responsable",
                table: "tareas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "tecnico_responsable",
                table: "tareas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "coordinacion_comentario",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "estado_comentario",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "duracion_hs",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "coordinacion_estado",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "requiere_coordinacion",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "coordinacion_responsable",
                table: "tareas");

            migrationBuilder.DropColumn(
                name: "tecnico_responsable",
                table: "tareas");

            /*migrationBuilder.AlterColumn<int>(
                name: "tipo",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "estado",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "contratista",
                table: "tareas",
                nullable: false,
                oldClrType: typeof(string));*/
        }
    }
}

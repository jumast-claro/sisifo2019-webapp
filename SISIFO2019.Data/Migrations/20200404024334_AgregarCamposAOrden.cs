﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SISIFO2019.Data.Migrations
{
    public partial class AgregarCamposAOrden : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "instfo_responsable_id",
                table: "ordenes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ticket_id",
                table: "ordenes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ordenes_instfo_responsable_id",
                table: "ordenes",
                column: "instfo_responsable_id");

            migrationBuilder.AddForeignKey(
                name: "FK_ordenes_instfo_integrantes_instfo_responsable_id",
                table: "ordenes",
                column: "instfo_responsable_id",
                principalTable: "instfo_integrantes",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ordenes_instfo_integrantes_instfo_responsable_id",
                table: "ordenes");

            migrationBuilder.DropIndex(
                name: "IX_ordenes_instfo_responsable_id",
                table: "ordenes");

            migrationBuilder.DropColumn(
                name: "instfo_responsable_id",
                table: "ordenes");

            migrationBuilder.DropColumn(
                name: "ticket_id",
                table: "ordenes");
        }
    }
}

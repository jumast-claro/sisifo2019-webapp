
const { src, dest } = require('gulp');

const merge = require("merge-stream");

const { series } = require('gulp');


let lib_root = "wwwroot/lib/";
let node_modules = "node_modules/";



function gulp_jquery(){
    return src(node_modules + "jquery/dist/jquery.js")
        .pipe(dest(lib_root + "jquery/dist/"));
    
}

function gulp_jqueryVal(){
    return src(node_modules + "jquery-validation/dist/jquery.validate.js")
        .pipe(dest(lib_root + "jquery-validation/dist"));

}

function gulp_jqueryValUn(){
    return src(node_modules + "jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.js")
        .pipe(dest(lib_root + "jquery-validation-unobtrusive/"));

}

function gulp_bcss() {
    return src(node_modules + "bootstrap/dist/css/*.css")
        .pipe(dest(lib_root + "bootstrap/dist/css")); 
}

function gulp_bjs(){
    return src(node_modules + "bootstrap/dist/js/*.js")
        .pipe(dest(lib_root + "bootstrap/dist/js"));
}


function gulp_moment(){
    return src(node_modules + "moment/moment.js")
        .pipe(dest(lib_root + "moment"));
}

function gulp_moment_loc(){
    return src(node_modules + "moment/min/moment-with-locales.js")
        .pipe(dest(lib_root + "moment"));
}

function gulp_axios(){
    return src(node_modules + "axios/dist/axios.js")
        .pipe(dest(lib_root + "axios")); 
}

function gulp_vue_bootstrap_datepicker(){
    return src(node_modules + "vue-bootstrap-datetimepicker/dist/vue-bootstrap-datetimepicker.js")
        .pipe(dest(lib_root + "vue-bootstrap-datetimepicker"));
}

function _vue(){
    return src(node_modules + "vue/dist/vue.js")
        .pipe(dest(lib_root + "vue")); 
}

function _vue_esm(){
    return src(node_modules + "vue/dist/vue.esm.browser.js")
        .pipe(dest(lib_root + "vue"));
}


exports.default = series(gulp_jquery, gulp_bcss, gulp_bjs, gulp_moment, gulp_jqueryVal, gulp_jqueryValUn, gulp_moment_loc, gulp_axios, gulp_vue_bootstrap_datepicker, _vue, _vue_esm);

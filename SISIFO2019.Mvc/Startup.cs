﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Api.Instalaciones.Validators;
using SISIFO2019.Mvc.Api.Ordenes.Models;
using SISIFO2019.Mvc.Api.Ordenes.Validators;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Validators;
using SISIFO2019.Mvc.Models.Instalaciones;
using SISIFO2019.Mvc.Models.Tareas;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("es-AR");
                options.SupportedCultures = new List<CultureInfo> { new CultureInfo("en-US"), new CultureInfo("es-AR") };
            });


            services.AddMvc(options => options.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);


            var mySqlConnection = @"server=localhost;Port=3306;Database=instfo;user=root;password=MySQLUDR151119";
            var datos_geograficos_connection =
                @"server=localhost;Port=3306;Database=datos_geograficos;user=root;password=MySQLUDR151119";
            services.AddDbContext<InstalacionesContext>(options => options.UseMySQL(mySqlConnection));
            services.AddDbContext<DatosGeograficosContext>(options => options.UseMySQL(datos_geograficos_connection));

            services.AddTransient<IValidator<OrdenCRM>, OrdenValidator>();
            services.AddTransient<IValidator<CrearTareaViewModel>, CrearTareaValidator>();
            services.AddTransient<IValidator<CrearInstalacionViewModel>, CrearInstalacionValidator>();
//            services.AddTransient<ClienteValidator, ClienteValidator>();
//            services.AddTransient<FechaInicioValidator, FechaInicioValidator>();
//            services.AddTransient<CompromisoValidator, CompromisoValidator>();
//            services.AddTransient<IValidator<EditarDatosBasicosViewModel>, EditarDatosBasicosValidator>();
//            services.AddTransient<IValidator<DatosGeograficosViewModel>, DatosGeograficosValidator>();
            
            services.AddTransient<IValidator<PostOrdenModel>, PostOrdenValidator>();
            services.AddTransient<IValidator<PostUbicacionGeograficaModel>, PostUbicacionGeograficaValidator>();
            services.AddTransient<IValidator<PostInstalacionesModel>, PostInstalacionValidator>();
            services.AddTransient<IValidator<PutInstalacionModel>, InstalacionValidator>();
            services.AddTransient<IValidator<UpdateOrdenModel>, UpdateOrdenValidator>();
            
            services.AddCors(options =>
            {
                options.AddPolicy("my_policy",
                    builder => { builder.WithOrigins("http://localhost:8080", "http://localhost:8081", "http://localhost:5000").AllowAnyHeader().AllowAnyMethod(); });
            });

            services.Configure<ApiBehaviorOptions>(setupAction =>
            {
                setupAction.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Title = "One or more model validations erros ocurred",
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "See the errors property for details",
                        Instance = context.HttpContext.Request.Path
                    };
                    
                    problemDetails.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);
                    
                    return new UnprocessableEntityObjectResult(problemDetails)
                    {
                        ContentTypes = {"application/problems+json"}
                    };
                };
            });

//            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseCors("my_policy");

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseRequestLocalization();


            app.UseStaticFiles();
//            app.UseStaticFiles(new StaticFileOptions()
//            {
//                FileProvider =
//                    new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(),
//                        "Areas/Instalaciones/Views")),
//                RequestPath = "/InstalacionesStaticFiles",
//            });


            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
//                routes.MapAreaRoute(
//                    name: "AreaInstalaciones",
//                    areaName: "Instalaciones",
//                    template: "Instalaciones/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

           
        }
    }
}
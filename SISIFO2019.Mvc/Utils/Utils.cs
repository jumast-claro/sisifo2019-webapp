using System;
using System.Globalization;

namespace SISIFO2019.Mvc.Validators
{
    public static class Utils
    {
        public static bool IsValidDateRepresentation(string fechaInicio)
        {
            return DateTime.TryParseExact(fechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out _);
        }

        public static DateTime ParseDateExact(string s)
        {
             var date = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture);
             return date;
        }
        
        public static DateTime? ParseNullableDateExact(string s)
        {

            if (string.IsNullOrEmpty(s))
            {
                return null;
            }
            else
            {
                return ParseDateExact(s);
            }
            
        }

        public static string DateToString(DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }

        public static string DateToString(DateTime? date)
        {
            return date.HasValue ? DateToString(date.Value) : "";
        }
        
        public static DateTime? ParseNullableDateTimeExact(string s)
        {

            if (string.IsNullOrEmpty(s))
            {
                return null;
            }
            else
            {
                var date = DateTime.ParseExact(s, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                return date;
            }
            
        }
        
        
        public static string DateTimeToString(DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture) : "";
        }
        
        
        
        

    }
}
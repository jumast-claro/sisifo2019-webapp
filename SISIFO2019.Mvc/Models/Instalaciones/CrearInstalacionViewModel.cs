using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Models.Instalaciones
{
    public class CrearInstalacionViewModel
    {
        public string Cliente { get; set; }


        public string FechaInicio { get; set; }

        public int DiasDeInstalacion { get; set; }

        public string FechaCompromiso
        {
            get
            {
                DateTime fecha;
                if (DateTime.TryParseExact(this.FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out fecha))
                {
                    var fechaCompromiso = fecha.AddDays(this.DiasDeInstalacion);
                    return fechaCompromiso.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public Etapa Etapa { get; set; }

        public Estado Estado { get; set; }

        public Responsable Responsable { get; set; }

        public int? DuenioId { get; set; }

        public string ProvinciaId { get; set; }

        public string Direccion { get; set; }

        public string DepartamentoId { get; set; }
        
        public string MunicipioId { get; set; }
        
        public string LocalidadId { get; set; }

        public string CalleId { get; set; }
        
        public int? CalleNumero { get; set; }
        
        public int? DireccionPiso { get; set; }

        public double? Latitud { get; set; }

        public double? Longitud { get; set; }

        public List<Provincia> ProvinciaOpciones { get; set; }

        public List<Municipio> MunicipioOpciones { get; set; }

        public List<IntegranteMdD> PMAsignadoOpciones { get; set; }

        public List<IntegranteInstFO> DuenioOpciones { get; set; }

        public string ClienteFinal { get; set; }


        public int? PMAsignadoId { get; set; }

        public bool EstaPriorizada { get; set; }


        public string Nota { get; set; }

        public string NombreProvincia { get; set; }
        
        
//        public string OrdenCliente { get; set; }
        
        public string NumeroDeOrden { get; set; }
        
        public string NumeroDeEnlace { get; set; }
        
//        public string OrdenFechaInicio { get; set; }
        
//        public int OrdenDiasDeInstalacion { get; set; }
        
        public TipoDeOrden TipoDeOrden { get; set; }
        
        public int CRM { get; set; }

        public bool RedirectedWithValidationErrors { get; set; } = true;


    }
}
﻿namespace SISIFO2019.Mvc.Models.Tareas
{
    public enum PeriodoTarea
    {
        Pasado,
        Ayer,
        Hoy,
        Manana,
        Futuro
    }
}
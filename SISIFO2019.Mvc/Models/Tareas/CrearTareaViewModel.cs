﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Models.Tareas
{
    public class CrearTareaViewModel
    {
        public int Id { get; set; }

        public int InstalacionId { get; set; }

        public TipoDeTarea Tipo { get; set; }

        public Contratista Contratista { get; set; }

        public EstadoDeTarea Estado { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime? FechaInicio { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime? FechaFin { get; set; }

        public int DuracionHoras { get; set; }

        public int DuracionMinutos { get; set; }

        public string Descripcion { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Models.Tareas
{
    public class TareaViewModel
    {
        private readonly Tarea _tarea;

        public TareaViewModel(Tarea tarea)
        {
            _tarea = tarea;
        }

        public int Id => _tarea.Id;

        public int InstalacionId => _tarea.InstalacionId;

        public DateTime? FechaInicio => _tarea.FechaInicio;

        public DateTime? FechaFin => _tarea.FechaFin;

        public TipoDeTarea Tipo => _tarea.Tipo;

        public EstadoDeTarea Estado => _tarea.Estado;

        public Contratista Contratista => _tarea.Contratista;

        public PeriodoTarea Periodo
        {
            get
            {
                var fechaTarea = this.FechaInicio.Value.Date;
                var hoy = DateTime.Today;


                if (fechaTarea < hoy.AddDays(-2))
                {
                    return PeriodoTarea.Pasado;
                }

                else if (fechaTarea < hoy)
                {
                    return PeriodoTarea.Ayer;
                }

                else if (fechaTarea == hoy)
                {
                    return PeriodoTarea.Hoy;
                }

                else if (fechaTarea < hoy.AddDays(2))
                {
                    return PeriodoTarea.Manana;
                }

                return PeriodoTarea.Futuro;
            }
        }
    }
}

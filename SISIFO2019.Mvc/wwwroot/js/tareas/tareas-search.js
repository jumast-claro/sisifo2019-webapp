﻿
// Input
let Input = function (selector) {
    this._$input = $(selector);
}

Input.prototype.enable = function () {
    //this._$input.attr('readonly', false);
    this._$input.removeAttr('readonly');
};

Input.prototype.disable = function () {
    this._$input.val("");
    this._$input.attr('readonly', 'readonly');
};

Input.prototype.val = function () {
    return this._$input.val();
};

Input.prototype.isEmpty = function () {
    return this._$input.val() == "";
};

Input.prototype.onEmpty = function(callback) {

    let self = this;
    this._$input.keyup(function() {
        if (self.isEmpty()) {
            callback();
        }

    });

};

Input.prototype.onEnterKeyPressed = function (callback) {
    this._$input.keyup(function (e) {
        // if ENTER
        if (e.keyCode == 13) {
            callback();
        }
    });
};

// Button
let Button = function (selector) {
    this._$button = $(selector);
}

Button.prototype.enable = function () {
    this._$button.attr('disabled', false);
};

Button.prototype.disable = function () {
    this._$button.attr('disabled', true);
};

Button.prototype.onClick = function (callback) {
    this._$button.on('click', callback);
}

// Select
let Select = function (selector) {
    this._$select = $(selector);
    this._selector = selector;
}

Select.prototype._$selectedOption = function () {
    return $(this._selector + " " + "option:selected");
};

Select.prototype.selectedValue = function () {
    return this._$selectedOption().val();
};

Select.prototype._showAllSelected = function () {
    return this._$selectedOption().attr('value') == -1;
};

Select.prototype.onShowAll = function (callback) {
    let self = this;
    this._$select.on('change',
        function () {
            if (self._showAllSelected()) {
                callback();
            }
        });
};

Select.prototype.onSearch = function (callback) {
    let self = this;
    this._$select.on('change',
        function () {
            if (!self._showAllSelected()) {
                callback();
            }
        });
};

// Search
let Search = function (selectSelector, inputSelector, buttonSelector, dataGrid) {

    this._select = new Select(selectSelector);
    this._input = new Input(inputSelector);
    this._button = new Button(buttonSelector);
    this._dataGrid = dataGrid;

    const self = this;

    this._button.onClick(function () {
        self.apply();
    });

    this._input.onEnterKeyPressed(function () {
        self.apply();
    });

    this._input.onEmpty(function() {
        self._dataGrid.showAllRows();
    });

    this._select.onShowAll(function () {
        self._dataGrid.showAllRows();
        self.disableSearch();
    });

    this._select.onSearch(function () {
        self.enableSearch();
    });
}

Search.prototype.enableSearch = function () {
    this._input.enable();
    this._button.enable();
};

Search.prototype.disableSearch = function () {
    this._input.disable();
    this._button.disable();
};

Search.prototype.apply = function () {
    this._dataGrid.filter(this._select.selectedValue(), this._input.val());
};

// DataGrid
let DataGrid = function () {

    let self = this;
    this.CLASS_NAME = "oculta";
    this.groups = [];

    const $groups = $('ol');
    $groups.each(function (group_index) {

        let group_info = {
            $group: this,
            index: group_index,
            rows: []
        }

        const rows = $(this).children();
        $(rows).each(function () {
            group_info.rows.push(this);
        });

        self.groups.push(group_info);
    });

};

DataGrid.prototype.iterGroups = function (callback) {
    this.groups.map(group => callback(group));
};

DataGrid.prototype.IterRows = function (callback) {
    for (let i = 0; i < this.groups.length; i++) {
        const rows = this.groups[i].rows;
        for (let j = 0; j < rows.length; j++) {
            const row = rows[j];
            callback(row);
        }
    }
};

DataGrid.prototype.showAllRows = function () {
    let self = this;
    self.IterRows(function (row) {
        self.show(row);
    });
    self.fixCounts();
};

DataGrid.prototype.filter = function (fieldIndex, searchInput) {

    let self = this;

    if (searchInput === "") {
        self.showAllRows();
    } else {
        this.IterRows(function (row) {
            let div = ($(row).children())[fieldIndex];
            let c = $(div).text();
            if (!c.includes(searchInput)) {
                self.hide(row);
            }
        });
    }
    self.fixCounts();
};

DataGrid.prototype.fixCounts = function () {
    this.iterGroups(function (group) {
        let self = $(group.$group);
        let items = $(group.$group).children();
        let all = items.length;
        let hidden = $(items).filter('.oculta').length;
        let visible = all - hidden;

        let parent = self.parent().parent();
        let span = $(parent).find('.count');
        $(span).html('(' + visible + ')');
    });
};

DataGrid.prototype.hide = function (row) {
    $(row).addClass(this.CLASS_NAME);
};

DataGrid.prototype.show = function (row) {
    $(row).removeClass(this.CLASS_NAME);
};

$(document).ready(function () {
    const dataGrid = new DataGrid();
    const searchViewModel = new Search('#search-select', '#search-input', '#search-button', dataGrid);
});

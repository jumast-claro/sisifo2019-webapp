﻿
$(document).ready((function () {

    let collapsableCount = $('.show').length;

    let contraerTodoButton = $("#contraer-todo");
    let expandirTodoButton = $("#expandir-todo");

    let allAreHidden = function () {

        let allHidden = $(".show");
        return allHidden.length == 0;
    }

    let allAreVisible = function () {

        let visibleCount = $('.show').length;
        return visibleCount == collapsableCount;
    }

    expandirTodoButton.attr("disabled", true);

    $("#contraer-todo").click(function contraerTodoOnClick() {

        $("div[id*=ol-wrapper]").collapse('hide');
        expandirTodoButton.attr("disabled", false);
        contraerTodoButton.attr("disabled", true);

    });

    $("#expandir-todo").click(function expandirTodoOnClick() {

        $("div[id*=ol-wrapper]").collapse('show');
        expandirTodoButton.attr("disabled", true);
        contraerTodoButton.attr("disabled", false);

    });

    $('div[id*=ol-wrapper]').on('hidden.bs.collapse', function () {
        expandirTodoButton.attr("disabled", false);
        if (allAreHidden()) {
            contraerTodoButton.attr("disabled", true);
        }

    });

    $('div[id*=ol-wrapper]').on('shown.bs.collapse', function () {

        contraerTodoButton.attr("disabled", false);

        if (allAreVisible()) {
            expandirTodoButton.attr("disabled", true);
        }
    });

    $("#invertir-visibilidad").click(function invertirVisibilidadOnClick() {

        $("div[id*=ol-wrapper]").collapse('toggle');

    });

    $('a[data-index]').click(function ordenar() {

        let self = this;
        let sortedColumn = 0;

        let index = Number($(this).attr("data-index"));

        let $lists = $('ol');
        $lists.each(function () {

            let $list = $(this);
            let listDOM = $list[0];

            if (index == self.sortedColumn) {
                let list_items = Array.from(listDOM.children).reverse();
                let $list_items = $(list_items);
                $list.empty();
                $list_items.appendTo(listDOM);

            } else {
                let list_items = ordenarLista($list, index);
                $list.empty();
                list_items.appendTo(listDOM);
                self.sortedColumn = index;
            }

        });
    });

    var ordenarLista = function ($list, divIndex) {

        let $list_items = $list.children();
        let li = [1, 2, 3];

        $list_items.sort(function (a, b) {
            let div_a = $(a).children()[divIndex];
            let div_b = $(b).children()[divIndex];
            return div_a.textContent < div_b.textContent ? -1 : 1;
        });

        return $list_items;
    }

}));
import {EstadoConverter, EtapaConverter, ResponsableConverter} from "../enumConverter.js";

import{ CrearUbicacion } from "./Ubicacion.js"

class InstalacionViewModel{

    constructor(data){

        this.Id = data.id;
        this.Cliente = data.cliente;
        this._etapaId = data.etapa;
        this._responsableId = data.responsable;
        this._estadoId = data.estado;
        this.CantidadOrdenes = data.ordenes.length;
        this.CantidadTareas = data.tareas.length;
        this.Duenio = data.duenio;
        this.Ubicacion = CrearUbicacion(data.ubicacion, data.direccion);
        this.Estado = EstadoConverter(data.estado);
        this.Etapa = EtapaConverter(data.etapa);
        this.Responsable = ResponsableConverter(data.responsable);
        this.Municipio = this.Ubicacion.Municipio;
        this.DireccionCompleta = this.Ubicacion.DireccionCompleta;

    }

}

export { InstalacionViewModel }
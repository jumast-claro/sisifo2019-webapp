
class Ubicacion{

    constructor(ubicacion, direccion){
        this.Direccion = direccion;
        this.CalleId = ubicacion.calleId;
        this.CalleNombre = ubicacion.calleNombre;
        this.CalleNumero = ubicacion.calleNumero;
        this.Latitud = ubicacion.latitud;
        this.Longitud = ubicacion.longitud;
    }

    _direccionEstaNormalizada(){
        return this.CalleId != null;
    }

    direccion(){
        return this._direccionEstaNormalizada() ? `${this.CalleNombre} ${this.CalleNumero}` : this.Direccion;
    }
}

class UbicacionCABA extends Ubicacion{

    constructor(ubicacion, direccion){
        super(ubicacion, direccion);
        this.Municipio = "C.A.B.A.";
    }

    get DireccionCompleta(){
        return `${this.direccion()}, C.A.B.A.`;
    }

}

class UbicacionBuenosAires extends Ubicacion{

    constructor(ubicacion, direccion){
        super(ubicacion, direccion);
        this.DepartamentoId = ubicacion.departamentoId;
        this.DepartamentoNombre = ubicacion.departamentoNombre;
        this.Municipio = ubicacion.departamentoNombre;
    }

    get DireccionCompleta(){
        return `${this.direccion()}, ${this.DepartamentoNombre}`;
    }
}

function CrearUbicacion(ubicacion, direccion) {

    if(ubicacion.provinciaId === "02"){
        return new UbicacionCABA(ubicacion, direccion);
    }
    else{
        return new UbicacionBuenosAires(ubicacion, direccion);
    }

}

export { CrearUbicacion }

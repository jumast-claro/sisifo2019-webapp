let provincia_selector = "#provincia_id";
let departamento_selector = "#departamento_id";
let municipio_selector = "#municipio_id";
let localidad_selector = "#localidad_id";
let calle_selector = "#calle_id";
let direccion_selector = "#direccion";
let latitud_selector = "#lat";
let longitud_selector = "#lon";


let normalizacionBtn_selector = "#normalizacion";

let $provincia_select = $(provincia_selector);
let $departamento_select = $(departamento_selector);
let $municipio_select = $(municipio_selector);
let $localidad_select = $(localidad_selector);
let $calle_select = $(calle_selector);
let $latitud_input = $(latitud_selector);
let $longitud_input = $(longitud_selector);

let $direccion_input = $(direccion_selector);
let $normalizacion_btn = $(normalizacionBtn_selector);

let dummy_option = `<option value="0" selected="selected" disabled="disabled" hidden="hidden">Seleccionar</option>`;

let CalleId = 0;
let CalleNombre = "";
let Latitud = 0;
let Longitud = 0;

let Calles = [];



let redirected = $("redirected").val();
if(redirected){
    
    resetAll();
}

$(function () {

    $("#modal_accept").on("click", function () {

        $localidad_select.attr('disabled', false);
        $calle_select.attr('disabled', false);

        let provincia_id = $(provincia_selector + " " + "option:selected").val();
        let calle_id = $("#modal_calle_id").val();

        let latitud = $("#latitud").val();
        let longitud = $("#longitud").val();

        $.ajax({
            method: "GET",
            url: "https://apis.datos.gob.ar/georef/api/ubicacion",
            data: {"lat": latitud, "lon": longitud}
        }).done(function (response) {


            let ubicacion = response.ubicacion;
            let departamento = ubicacion.departamento;
            let municipio = ubicacion.municipio;

            $departamento_select.empty();
            $municipio_select.empty();

            let departamento_opcion = `<option value="${departamento.id}">${departamento.nombre}</option>`;
            $departamento_select.append(departamento_opcion);

            if (municipio.id !== null) {
                let municipios_opcion = `<option value="${municipio.id}">${municipio.nombre}</option>`;
                $municipio_select.append(municipios_opcion);

                $.ajax({
                    method: "GET",
                    url: "https://apis.datos.gob.ar/georef/api/localidades",
                    data: {"municipio": municipio.id, "max": 100}
                }).done(function (response) {

                    $localidad_select.empty();
                    $localidad_select.append(dummy_option);

                    let localidades = response.localidades;
                    for (let i = 0; i < localidades.length; i++) {

                        let localidad = localidades[i];
                        let localidad_opcion = `<option value="${localidad.id}">${localidad.nombre}</option>`;
                        $localidad_select.append(localidad_opcion);
                    }


                });
            }

            $("#lat").val($("#latitud").val());
            $("#lon").val($("#longitud").val());
            
            updateMap();

        });


        $.ajax({
            method: "GET",
            url: "https://apis.datos.gob.ar/georef/api/calles",
            data: {"id": calle_id, "max": 5000}
        }).done(function (response) {


            let calle = response.calles[0];
            console.log(calle);

            let altura_inicio = Math.min(calle.altura.inicio.izquierda, calle.altura.inicio.derecha);
            let altura_fin = Math.max(calle.altura.fin.izquierda, calle.altura.fin.derecha);
            let calle_option = `<option value="${calle.id}">${calle.nombre} [${altura_inicio}-${altura_fin}] - ${calle.departamento.nombre}</option>`;

            $calle_select.append(calle_option);

            console.log(calle)

        });
        

    });

});

$(function () {

    $normalizacion_btn.on("click", function () {

        Calles = [];

        let $body = $("#modal_results");
        $body.empty();

        $("#latitud").val("");
        $("#longitud").val("");
        $("#modal_calle_id").val("");
        $("#modal_calle_nombre").val("");


        let provincia = $(provincia_selector + " " + "option:selected").val();
        let departamento = $(departamento_selector + " " + "option:selected").val();
        let direccion = $direccion_input.val();
        
        data = {
           provincia: provincia,
           direccion: direccion 
        };

        let departamento_value = $(departamento_selector + " " + "option:selected").attr("value");
        if(departamento_value != 0){
            data.departamento = departamento;
        }

        $.ajax({
            method: "GET",
            url: "https://apis.datos.gob.ar/georef/api/direcciones",
            data: data
        }).done(function (response) {

            $calle_select.empty();
            for (let i = 0; i < response.cantidad; i++) {

                let direccion = response.direcciones[i];
                let row = `ID: "${direccion.calle.id}" - ${direccion.calle.nombre} - ${direccion.departamento.nombre}, ${direccion.provincia.nombre} / ${direccion.ubicacion.lat}, ${direccion.ubicacion.lon}`;

                Calles.push({
                    Fila: i,
                    CalleId: direccion.calle.id,
                    CalleNombre: direccion.calle.nombre,
                    Latitud: direccion.ubicacion.lat,
                    Longitud: direccion.ubicacion.lon,

                });

                let div = `<tr>
                                <td>${direccion.calle.nombre}</td>
                                <td>${direccion.altura.valor}</td>
                                <td>${direccion.departamento.nombre}</td>
                                <td>${direccion.calle.id}</td>
                                <td><button class="test pt-0 pb-0 btn btn-secondary btn-sm" data-pos=${i}>OK</button>
                                </td>
                           </tr>`;

                $body.append(div);
            }

        });

        $(document).on("click", ".test", function () {

            let pos = $(this).attr("data-pos");
            let Calle = Calles[pos];
            let lat = Calle.Latitud;
            let lon = Calle.Longitud;
            let calle_id = Calle.CalleId;
            let calle_nombre = Calle.CalleNombre;


            $("#modal_calle_id").val(calle_id);
            $("#modal_calle_nombre").val(calle_nombre);
            $("#latitud").val(lat);
            $("#longitud").val(lon);

        });

    });

});


function getProvinciaSeleccionadaId() {
    return $(provincia_selector + " " + "option:selected").val();
}

function updateDepartamentos(provincia_id) {

    $departamento_select.attr('disabled', false);
    $departamento_select.empty();
    $departamento_select.append(dummy_option);

    $.ajax({
        method: "GET",
        url: "https://apis.datos.gob.ar/georef/api/departamentos",
        data: {"provincia": provincia_id, "max": 1000, "orden": "id"}
    }).done(function (response) {

        let departamentos = response.departamentos;
        for (let i = 0; i < response.cantidad; i++) {
            let departamento = departamentos[i];
            let departamento_option = `<option value="${departamento.id}">${departamento.nombre} - ${departamento.id}</option>`;
            $departamento_select.append(departamento_option);
        }

    });

}

function updateMunicipio(provincia_id) {

    $municipio_select.attr('disabled', false);
    $municipio_select.empty();
    $municipio_select.append(dummy_option);

    if (provincia_id == 2) {
        return;
    }

    $.ajax({
        method: "GET",
        url: "https://apis.datos.gob.ar/georef/api/municipios",
        data: {"provincia": provincia_id, "max": 1000, "orden": "nombre"}
    }).done(function (response) {

        let municipios = response.municipios;
        for (let i = 0; i < response.cantidad; i++) {
            let municipio = municipios[i];
            let option = `<option value="${municipio.id}">${municipio.nombre} - ${municipio.id}</option>`;
            $municipio_select.append(option);
        }

    });
}

function updateLocalidadesConDepartamento(provincia_id, departamento_id) {

    $localidad_select.attr('disabled', false);
    $localidad_select.empty();
    $localidad_select.append(dummy_option);

    if (provincia_id == 2) {
        return;
    }


    $.ajax({
        method: "GET",
        url: "https://apis.datos.gob.ar/georef/api/localidades",
        data: {"departamento": departamento_id, "max": 1000}
    }).done(function (response) {


        let localidades = response.localidades;
        for (let i = 0; i < localidades.length; i++) {

            let localidad = localidades[i];
            let localidad_opcion = `<option value="${localidad.id}">${localidad.nombre}</option>`;
            $localidad_select.append(localidad_opcion);
        }


    });
}

function updateCallesConProvincia(provincia_id) {

    $calle_select.attr('disabled', false);
    $calle_select.empty();
    $calle_select.append(dummy_option);

    $.ajax({
        method: "GET",
        url: "https://apis.datos.gob.ar/georef/api/calles",
        data: {"provincia": provincia_id, "max": 5000, "orden": "nombre"}
    }).done(function (response) {

        let calles = response.calles;
        console.log(calles);
        for (let i = 0; i < response.cantidad; i++) {

            let calle = calles[i];

            let alturaFin = Math.min(calle.altura.fin.derecha, calle.altura.fin.izquierda);
            let alturaInicio = Math.min(calle.altura.inicio.derecha, calle.altura.inicio.izquierda);

            let option = `<option value="${calle.id}">${calle.nombre} [${alturaInicio}-${alturaFin}] - ${calle.departamento.nombre}</option>`;
            $calle_select.append(option);
        }

    })
}


function updateCallesConDepartamento(departamento_id) {

    $calle_select.attr('disabled', false);
    $calle_select.empty();
    $calle_select.append(dummy_option);

    let $selected_municipio = $(municipio_selector + " " + "option:selected");
    let selected_municipio_value = $selected_municipio.val();

    let departamentoSelecctionadoId = $(departamento_selector + " " + "option:selected").val();

    $.ajax({
        method: "GET",
        url: "https://apis.datos.gob.ar/georef/api/calles",
        data: {"departamento": departamentoSelecctionadoId, "max": 5000, "orden": "nombre"}
    }).done(function (response) {


        let calles = response.calles;
        for (let i = 0; i < response.cantidad; i++) {

            let calle = calles[i];

            let alturaFin = Math.min(calle.altura.fin.derecha, calle.altura.fin.izquierda);
            let alturaInicio = Math.min(calle.altura.inicio.derecha, calle.altura.inicio.izquierda);

            let option = `<option value="${calle.id}">${calle.nombre} [${alturaInicio}-${alturaFin}] - ${calle.departamento.nombre}</option>`;
            $calle_select.append(option);
        }

    })
}

function resetAll() {
    
    $departamento_select.attr('disabled', true);
    $municipio_select.attr('disabled', true);
    $localidad_select.attr('disabled', true);
    $calle_select.attr('disabled', true);
    // $latitud_input.prop('disabled', true);
    // $longitud_input.prop('disabled', true);
    $departamento_select.empty();
    $departamento_select.append(dummy_option);
    $municipio_select.empty();
    $municipio_select.append(dummy_option);
    $localidad_select.empty();
    $localidad_select.append(dummy_option);
    $calle_select.empty();
    $calle_select.append(dummy_option);

    $latitud_input.val("");
    $longitud_input.val("");
}

$(function () {

    $provincia_select.on("change", function () {

        let provincia_id = $(provincia_selector + " " + "option:selected").val();
        if (provincia_id == 0) {
            return;
        }

        resetAll();
        // $latitud_input.prop('disabled', true);
        // $longitud_input.prop('disabled', true);
        updateDepartamentos(provincia_id);

        // Si es C.A.B.A.
        if (provincia_id == 2) {
            updateCallesConProvincia(provincia_id);
        } else {
            updateMunicipio(provincia_id);
        }
    });

    $departamento_select.on("change", function () {

        let provincia_id = $(provincia_selector + " " + "option:selected").val();
        let departamento_id = $(departamento_selector + " " + "option:selected").val();

        updateCallesConDepartamento(departamento_id);

        // Si no es C.A.B.A.
        if (provincia_id != 2) {
            updateLocalidadesConDepartamento(provincia_id, departamento_id);
        }
    });

    $calle_select.on("change", function () {

        let provincia_id = $(provincia_selector + " " + "option:selected").val();

        if (provincia_id != 2) {
            return;
        }

        let calle_id = $(calle_selector + " " + "option:selected").val();
        updateDeparamentosConCalle(calle_id);

    });

    $("#restablecer").on("click", function () {
        resetAll();
    })

});

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function update() {
    let $inicio = $("#inicio");
    let str = $inicio.val();

    let ini = $("#dias_inst").val();

    let m = moment(str, "DD/MM/YYYY");

    m.add(ini, "days");

    $("#compromiso").val(m.format("DD/MM/YYYY"));
}

$(function () {

    $("#dias_inst").on("change", function () {
        update();
    });


    $("#inicio").on("input", function () {
        update();
    });


    $(".collapse").on("show.bs.collapse", function () {


        let id = $(this).attr("id");
        let n = id.split('-')[1];

        $("#collapse-button-" + n).text("Ocultar")
    });

    $(".collapse").on("hide.bs.collapse", function () {

        let id = $(this).attr("id");
        let n = id.split('-')[1];
        $("#collapse-button-" + n).text("Mostrar")
    });

});

$(function () {

    $(".form-control").addClass("form-control-sm");
    $(".col-form-label").addClass("col-form-label-sm");

    $(".btn").add("btn-sm");

});

$(function () {
    $('#datetimepicker2, #datetimepicker3').datetimepicker({
        format: "DD/MM/YYYY",
        locale: "es"
    });

});

$(document).ready(function () {


    $('#mapa').bind('mousewheel DOMMouseScroll', function (e) { e.preventDefault(); });
    
    let mapa = $("#mapa").argenmap({
        mapaFijo: false,
    });
    mapa.centro(-34.6144934119689, -58.4458563545429);
    mapa.zoom(8);
    
    $("#google_maps").on("click", function () {

        let lat = parseFloat($("#lat").val());
        let lon = parseFloat($("#lon").val());
        let link = `https://www.google.com.ar/maps/@${lat},${lon},20z`
        
    });
    
    
    $("#lat, #lon").on("change", function () {
        updateMap();
    });
    
    $("#zoom_mas").on("click", function () {

        let lat = parseFloat($("#lat").val());
        let lon = parseFloat($("#lon").val());
        let mapa = $("#mapa");
        mapa.centro(lat, lon);
        mapa.zoom(mapa.zoom() + 2); 
        
    });

    $("#zoom_menos").on("click", function () {

        let lat = parseFloat($("#lat").val());
        let lon = parseFloat($("#lon").val());
        let mapa = $("#mapa");
        mapa.centro(lat, lon);
        mapa.zoom(mapa.zoom() - 2);

    });

    $("#zoom_normal").on("click", function () {

        let lat = parseFloat($("#lat").val());
        let lon = parseFloat($("#lon").val());
        let mapa = $("#mapa");
        mapa.centro(lat, lon);
        mapa.zoom(13);
        
    });

});

function updateMap() {

    let lat = parseFloat($("#lat").val());
    let lon = parseFloat($("#lon").val());
    let mapa = $("#mapa");

    mapa.zoom(13);
    mapa.centro(lat, lon);
    mapa.agregarMarcador(lat, lon);

    let link = `https://www.google.com.ar/maps/search/${lat},${lon}`;
    $("#google_maps").attr("href", link);
}
$(function () {
   
    $("#btn_calle_cancelar").on("click", function () {

        $('#calle_id option')
            .removeAttr('selected')
            .filter('[value=0]')
            .attr('selected', true)
        
    });
    
});

$(".label-for-required").addClass("font-weight-bold");


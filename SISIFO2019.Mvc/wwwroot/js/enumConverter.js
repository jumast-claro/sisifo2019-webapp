
function EtapaConverter(enumNumber){

    let values = {
        1: "Análisis",
        2: "Definición de proyecto",
        3: "Relevamiento",
        4: "Permisos",
        5: "Obra civil",
        6: "Tendido",
        7: "Empalmes",
        8: "Garantía",
        9: "Terminada"
    };

    return  values[enumNumber];

}

function ResponsableConverter(enumNumber) {
    
    let values = {
        1: "Comercial",
        2: "Consultoría & Diseño",
        3: "Mesa de Despacho",
        4: "PM",
        5: "Gestión de Ingresos",
        6: "Ingeniería",
        7: "Administración FO",
        8: "Permisos municipales",
        9: "Instalaciones FO",
        10: "Inserciones AMBA",
        11: "Start-Up",
        12: "Mantenimiento",
        13: "Contratista",
        14: "Cliente"
    };
    
    return values[enumNumber]
    
}

function EstadoConverter(enumNumber) {

    let values = {
        1: "En proceso",
        2: "Interrumpida",
        3: "Terminada"

    };

    return values[enumNumber];
}

export { EtapaConverter, ResponsableConverter, EstadoConverter }
﻿class Person {

    private _name: string;
    private _age: number;

    constructor(name: string, age: number) {
        this._name = name;
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    greet(): void {
        alert(this._name + " " + this._age);
    }

}

let person = new Person("Juan", 35);
let age: number = person.age;
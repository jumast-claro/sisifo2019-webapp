var Person = /** @class */ (function () {
    function Person(name, age) {
        this._name = name;
        this._age = age;
    }
    Object.defineProperty(Person.prototype, "age", {
        get: function () {
            return this._age;
        },
        set: function (value) {
            this._age = value;
        },
        enumerable: true,
        configurable: true
    });
    Person.prototype.greet = function () {
        alert(this._name + " " + this._age);
    };
    return Person;
}());
var person = new Person("Juan", 35);
var age = person.age;
//# sourceMappingURL=Person.js.map
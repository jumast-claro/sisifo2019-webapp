﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Validators
{
    public class OrdenValidator : AbstractValidator<OrdenCRM>
    {
        public OrdenValidator()
        {
            RuleFor(o => o.FechaInicio)
                .NotNull().WithMessage("El campo 'Fecha de inicio' es obligatorio.")
                .Must(f => f <= DateTime.Now).WithMessage("'Fecha de inicio' debe ser menor o igual a hoy.");
        }
    }
}

﻿    using System;
using System.Collections.Generic;
    using System.Data;
    using System.Linq;
using System.Threading.Tasks;
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using SISIFO2019.Mvc.Models.Tareas;

    namespace SISIFO2019.Mvc.Validators
{
    public class CrearTareaValidator : AbstractValidator<CrearTareaViewModel>
    {
        public CrearTareaValidator()
        {
            RuleFor(vm => vm.InstalacionId).GreaterThanOrEqualTo(1)
                .WithMessage("Error: InstalacionId debe ser mayor o igual que 1.");

            RuleFor(vm => vm.Tipo).IsInEnum()
                .WithMessage("Error: La opción seleccionada no corresponde a un valor válido.");

            RuleFor(vm => vm.Contratista).IsInEnum()
                .WithMessage("Error: La opción seleccionada no corresponde a un valor válido.");

            RuleFor(vm => vm.Estado).IsInEnum()
                .WithMessage("Error: La opción seleccionada no corresponde a un valor válido.");

            RuleFor(vm => vm.FechaInicio).NotNull()
                .WithMessage("Error: La fecha de inicio no puede estar vacía.");

            RuleFor(vm => vm.DuracionHoras).InclusiveBetween(1, 10)
                .WithMessage("Error: La cantidad de horas debe estar entre 1 y 10 (inclusive).");

            RuleFor(vm => vm.DuracionMinutos).InclusiveBetween(0, 59)
                .WithMessage("Error: La cantidad de minutos debe estar entre 0 y 59 (inclusive).");
        }
    }
}

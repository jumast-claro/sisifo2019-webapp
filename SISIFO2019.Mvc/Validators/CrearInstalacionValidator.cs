using System;
using System.Globalization;
using FluentValidation;
using SISIFO2019.Mvc.Models.Instalaciones;

namespace SISIFO2019.Mvc.Validators
{
    public class CrearInstalacionValidator : AbstractValidator<CrearInstalacionViewModel>
    {
        public CrearInstalacionValidator()
        {
            RuleFor(i => i.Cliente).NotNull().WithMessage("Campo obligatorio");
            RuleFor(i => i.Cliente).NotEmpty().WithMessage("Campo obligatorio");
            RuleFor(i => i.Cliente).MinimumLength(3).WithMessage("Longitud mínima: 3");
            RuleFor(i => i.Cliente).MaximumLength(125).WithMessage("Longitud máxima: 125");

            RuleFor(i => i.Direccion).NotNull().WithMessage("Campo obligatorio");
            RuleFor(i => i.Direccion).NotEmpty().WithMessage("Campo obligatorio");
            RuleFor(i => i.Direccion).MinimumLength(5).WithMessage("Longitud mínima: 5");
            RuleFor(i => i.Direccion).MaximumLength(125).WithMessage("Longitud máxima: 125");

            RuleFor(i => i.FechaInicio).Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Must(FechaDeInicioFormatoValido).WithMessage("Formato inválido. Debe ser DD/MM/AAAA")
                .Must(FechaDeInicioFechaValida).WithMessage("Debe ser menor o igual a hoy");

            RuleFor(i => i.DiasDeInstalacion).NotNull().WithMessage("Campo obligatorio");
            RuleFor(i => i.DiasDeInstalacion).NotEmpty().WithMessage("Campo obligatorio");
            RuleFor(i => i.DiasDeInstalacion).InclusiveBetween(35, 365)
                .WithMessage("Valor mínimo: 35 - Valor máximo: 365");


            RuleFor(i => i.ProvinciaId).NotNull().WithMessage("Campo obligatorio");
            RuleFor(i => i.ProvinciaId).NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(i => i.DepartamentoId).NotNull()
                .When(i => i.ProvinciaId != "02")
                .WithMessage("Campo obligatorio cuando provincia no es C.A.B.A.");


            RuleFor(i => i.NumeroDeOrden)
                .NotNull().WithMessage("Campo obligatorio cuando CRM es Vantive o Workflow Client")
                .MinimumLength(8).WithMessage("Longitud minima: 8")
                .MaximumLength(10).WithMessage("Longitud maxima: 10")
                .When(i => i.CRM != 0);

            RuleFor(i => i.NumeroDeEnlace)
                .NotNull()
                .MinimumLength(7).WithMessage("Longitud minima: 7")
                .MaximumLength(10).WithMessage("Longitud maxima: 10")
                .When(i => i.CRM != 0)
                .WithMessage("Campo obligatorio cuando CRM es Vantive o Workflow Client");
            
             RuleFor(i => i.TipoDeOrden).NotNull()
                 .When(i => i.CRM != 0)
                 .WithMessage("Campo obligatorio cuando CRM es Vantive o Workflow Client");

        }

        private static bool FechaDeInicioFormatoValido(string fechaInicio)
        {
            DateTime date;
            return DateTime.TryParseExact(fechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out date);
        }

        private static bool FechaDeInicioFechaValida(string fechaInicio)
        {
            var date = DateTime.ParseExact(fechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            return date <= DateTime.Now;
        }
    }
}
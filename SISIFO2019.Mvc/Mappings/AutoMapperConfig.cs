﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Models.Tareas;

namespace SISIFO2019.Mvc.Mappings
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<CrearTareaViewModel, Tarea>(); 
        }
    }
}

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Ordenes.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Ordenes.Controllers
{
    [Route("api/ordenes")]
    [ApiController]
    public class OrdenesController : ControllerBase
    {
        private readonly DatosGeograficosContext _datosGeograficosContext;
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public OrdenesController(InstalacionesContext instalacionesContext, DatosGeograficosContext datosGeograficosContext,LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _datosGeograficosContext = datosGeograficosContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<GetOrdenModel>>> Get()
        {
            try
            {
                var ordenes = await this._instalacionesContext.Ordenes
                    .Include(o => o.Instalacion)
                    .Include(o => o.ResponsableInstFO)
                    .ToListAsync();
                
                
                return Ok(ordenes.Select(o => new GetOrdenModel(o)));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<OrdenCRM>> Get(int id)
        {

            try
            {
                var orden = await this._instalacionesContext.Ordenes
                    .Where(o => o.Id == id)
                    .Include(o => o.Instalacion)
                    .Include(o => o.ResponsableInstFO)
                    .SingleOrDefaultAsync();

                if (orden == null)
                {
                    return NotFound();
                }

                return Ok(new GetOrdenModel(orden));
            }
            catch (Exception e)
            {
               return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message); 
            }

        }

        [HttpPost]
        public async Task<ActionResult<GetOrdenModel>> Post(PostOrdenModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                var inicio = DateTime.ParseExact(model.FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var compromiso = inicio.AddDays(model.CompromisoEnDias);
                
                var orden = new OrdenCRM()
                {
                    Cliente = new ClienteOrden()
                    {
                        RazonSocial = model.ClienteRazonSocial,
                        CUIT = model.ClienteCUIT,
                    },
                    
                    NumeroOrdenCRM = model.NumeroOrdenCRM,
                    NumeroEnlace = model.NumeroEnlace,
                    NumeroOrdenSAP = model.NumeroOrdenSAP,
                    
                    Ubicacion = new SitioInstalacionOrden()
                    {
                        ProvinciaId = model.ProvinciaId,
                        ProvinciaNombre = _datosGeograficosContext.Provincias.Single(p => p.Id == model.ProvinciaId).Nombre,
                        DepartamentoId = model.DepartamentoId,
                        DepartamentoNombre = this._datosGeograficosContext.Departamentos.SingleOrDefault(d => d.Id == model.DepartamentoId)?.Nombre,
                        Direccion = model.Direccion,
                        
                    },
                    
                    PmAsignado = new PMAsignadoAOrden()
                    {
                        Identificador = model.PMAsignadoId,
                        Nombre = this._instalacionesContext.PMs.Single(pm => pm.Id == model.PMAsignadoId).Nombre,
                        Apellido = this._instalacionesContext.PMs.Single(pm => pm.Id == model.PMAsignadoId).Apellido,
                    },
                    
                    FechaInicio = DateTime.ParseExact(model.FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    FechaCompromiso = compromiso,
//                    DiasDeInstalacion = model.CompromisoEnDias,
                    Tipo = model.TipoOrden,
                    CRM = model.CRM,
                    Comentarios = model.Comentarios,
                    UbicacionTecnica = model.UbicacionTecnica,
                    
                    TicketId = model.TicketId,
                    ResponsableInstFOId = model.ResponsableInstFOId
                    
                };


                var posted = this._instalacionesContext.Add(orden);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    "Ordenes",
                    values: new {id = posted.Entity.Id});

                var created = _instalacionesContext.Ordenes
                    .Include(o => o.ResponsableInstFO)
                    .Single(o => o.Id == posted.Entity.Id);
                return Created(url, new GetOrdenModel(created));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var orden = await this._instalacionesContext.Ordenes
                    .SingleOrDefaultAsync(o => o.Id == id);

                if (orden == null)
                {
                    return NotFound();
                }

                this._instalacionesContext.Remove(orden);
                await this._instalacionesContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int id, UpdateOrdenModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                 var inicio = DateTime.ParseExact(model.FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                 var compromiso = inicio.AddDays(model.Dias); 
                 var fechaCierre = Utils.ParseNullableDateExact(model.FechaCierre);

                var orden = await this._instalacionesContext.Ordenes
                    .Include(o => o.ResponsableInstFO)
                    .SingleOrDefaultAsync(o => o.Id == id);
                if (orden == null)
                {
                    return NotFound();
                }

                orden.InstalacionId = model.InstalacionID;
                
                orden.Cliente.RazonSocial = model.RazonSocial;
                orden.Cliente.CUIT = model.CUIT;

                orden.CRM = model.CRM;
                orden.NumeroOrdenCRM = model.NumeroOrdenCRM;
                orden.NumeroEnlace = model.NumeroEnlace;
                orden.NumeroOrdenSAP = model.NumeroOrdenSAP;
                orden.Tipo = model.TipoOrden;
                
                orden.Ubicacion.ProvinciaId = model.ProvinciaId;
                orden.Ubicacion.ProvinciaNombre =
                    _datosGeograficosContext.Provincias.Single(p => p.Id == model.ProvinciaId).Nombre;
                orden.Ubicacion.DepartamentoId = model.DepartamentoId;
                orden.Ubicacion.DepartamentoNombre = this._datosGeograficosContext.Departamentos
                    .SingleOrDefault(d => d.Id == model.DepartamentoId)?.Nombre;
                orden.Ubicacion.Direccion = model.Direccion;

                orden.FechaInicio = Utils.ParseDateExact(model.FechaInicio);
                orden.FechaCompromiso = compromiso;

                if (fechaCierre.HasValue && model.Resultado.HasValue)
                {
                    orden.Cierre.FechaCierre = fechaCierre.Value;
                    orden.Cierre.Resultado = model.Resultado.Value;
                }

                orden.Comentarios = model.Nota;
                orden.UbicacionTecnica = model.UbicacionTecnica;

                if (model.PmAsignadoId.HasValue)
                {
                    orden.PmAsignado.Identificador = model.PmAsignadoId.Value;
                    orden.PmAsignado.Nombre = this._instalacionesContext.PMs.Single(pm => pm.Id == model.PmAsignadoId).Nombre;
                    orden.PmAsignado.Apellido = this._instalacionesContext.PMs.Single(pm => pm.Id == model.PmAsignadoId).Apellido;

                }
                else
                {
                    orden.PmAsignado = null;
                }

                orden.TicketId = model.TicketId;
                orden.ResponsableInstFOId = model.ResponsableInstFOId;

                await _instalacionesContext.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
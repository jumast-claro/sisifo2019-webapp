namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class ClienteModel
    {

        public ClienteModel(string razonSocial, string cuit)
        {
            RazonSocial = razonSocial;
            CUIT = cuit;
        }
        
        public string RazonSocial { get; }
        public string CUIT { get;  }
    }
}
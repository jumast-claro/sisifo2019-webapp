namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class PMAsignadoModel
    {
        public PMAsignadoModel(int id, string nombre, string apellido)
        {
            Id = id;
            Nombre = nombre;
            Apellido = apellido;
        }
        
        public int Id { get; set; }
        
        public string Nombre { get; }
        public string Apellido { get; }
    }
}
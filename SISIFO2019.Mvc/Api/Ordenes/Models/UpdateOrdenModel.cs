using Google.Protobuf;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class UpdateOrdenModel
    {
        
        public int? InstalacionID { get; set; }

        public CRM CRM { get; set; }

        public string RazonSocial { get; set; }
        public string CUIT { get; set; }
        
        public string NumeroOrdenCRM { get; set; }
        public string NumeroEnlace { get; set; }
        public string NumeroOrdenSAP { get; set; }
        
        public string ProvinciaId { get; set; }
        public string DepartamentoId { get; set; }
        public string Direccion { get; set; }
        
        public string FechaInicio { get; set; }
        public int Dias { get; set; }
        
        public string FechaCierre { get; set; }
        
        public ResultadoOrden? Resultado { get; set; }
        
        public TipoDeOrden TipoOrden { get; set; }
        
        public string Nota { get; set; }
        
        public string UbicacionTecnica { get; set; }
        
        public int? PmAsignadoId { get; set; }
        
        public string TicketId { get; set; }
        
        public int? ResponsableInstFOId { get; set; }

    }
}
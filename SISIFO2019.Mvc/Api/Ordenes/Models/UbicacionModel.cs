using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class UbicacionModel
    {
        public UbicacionModel(string provinciaId, string provinciaNombre, string departamentoId, string departamentoNombre, string direccion)
        {
            ProvinciaId = provinciaId;
            ProvinciaNombre = provinciaNombre;
            DepartamentoId = departamentoId;
            DepartamentoNombre = departamentoNombre;
            Direccion = direccion;
        }
       
        public string ProvinciaId { get; }
        public string ProvinciaNombre { get; }
        public string DepartamentoId { get; }
        public string DepartamentoNombre { get; }
        public string Direccion { get; }
    }
}
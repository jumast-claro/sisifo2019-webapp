﻿using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class InstalacionModel
    {
        public InstalacionModel(Instalacion instalacion)
        {
            Id = instalacion.Id;
            Estado = instalacion.Estado;
            Etapa = instalacion.Etapa;
            Responsable = instalacion.Responsable;
            FechaTerminada = Utils.DateToString(instalacion.FechaTerminada);
        }
        
        public int Id { get; }
        
        public Estado Estado { get; }

        public Etapa? Etapa { get; }
        
        public Responsable? Responsable { get; }
        
        public string FechaTerminada { get; }
    }
}
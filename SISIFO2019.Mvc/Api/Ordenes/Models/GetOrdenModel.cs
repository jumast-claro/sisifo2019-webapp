using System;
using System.Diagnostics.CodeAnalysis;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class GetOrdenModel
    {
        public GetOrdenModel(OrdenCRM orden)
        {
            this.Id = orden.Id;
            this.InstalacionId = orden.InstalacionId;
         
            this.NumeroOrdenCRM = orden.NumeroOrdenCRM;
            this.NumeroEnlace = orden.NumeroEnlace;
            this.NumeroOrdenSAP = orden.NumeroOrdenSAP;
            this.Cliente = new ClienteModel(orden.Cliente.RazonSocial, orden.Cliente.CUIT);
            this.TipoOrden = new EnumModel<TipoDeOrden>(orden.Tipo);
            FechaInicio = Utils.DateToString(orden.FechaInicio);
//            FechaCompromiso = Utils.DateToString(orden.FechaInicio.AddDays(orden.DiasDeInstalacion));
            FechaCompromiso = Utils.DateToString(orden.FechaCompromiso);
//            FechaCierre = Utils.DateToString(orden.FechaCierre);
            FechaCierre = Utils.DateToString(orden.Cierre.FechaCierre);
            

            var ubicacion = orden.Ubicacion;
            this.Ubicacion = new UbicacionModel(ubicacion.ProvinciaId, ubicacion.ProvinciaNombre, ubicacion.DepartamentoId, ubicacion.DepartamentoNombre, ubicacion.Direccion);
            
            this.PMAsignado = new PMAsignadoModel(orden.PmAsignado.Identificador ,orden.PmAsignado.Nombre, orden.PmAsignado.Apellido);
            this.CRM = new EnumModel<CRM>(orden.CRM);
//            this.Resultado = orden.Resultado;
            this.Resultado = orden.Cierre.Resultado;

            this.UbicacionTecnica = orden.UbicacionTecnica;

            this.Comentarios = orden.Comentarios;
            
            this.Instalacion = orden.Instalacion != null ? new InstalacionModel(orden.Instalacion) : null;

            if (orden.ResponsableInstFO != null)
            {
                this.ResponsableInstFO = new PMAsignadoModel(orden.ResponsableInstFO.Id, orden.ResponsableInstFO.Nombre, orden.ResponsableInstFO.Apellido);
            }

            this.TicketId = orden.TicketId;

        }

        public int Id { get; }
        public int? InstalacionId { get; }

        public InstalacionModel Instalacion { get; set; }
        
        public EnumModel<CRM> CRM { get; } 
        
        public string NumeroOrdenCRM { get; }

        public string NumeroEnlace { get; }
        
        public string NumeroOrdenSAP { get; }

        public ClienteModel Cliente { get; }

        
        public PMAsignadoModel PMAsignado { get; }
        
        public EnumModel<TipoDeOrden> TipoOrden { get; }
        
        public UbicacionModel Ubicacion { get; }

        public string FechaInicio { get; }
        
        public string FechaCompromiso { get; }
        
        public string FechaCierre { get; }
        
        public ResultadoOrden? Resultado { get; set; }
        
        public string UbicacionTecnica { get; set; }
        
        public string Comentarios { get; set; }
        
        public string TicketId { get; set; }
        public PMAsignadoModel ResponsableInstFO { get; set; }
        
    }
}
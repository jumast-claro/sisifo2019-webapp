using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetOrdenesModel
    {
        public GetOrdenesModel(IReadOnlyCollection<OrdenCRM> ordenes)
        {
            Cantidad = ordenes.Count;
            Ordenes = ordenes.Select(o => new GetOrdenModel(o)).ToList();
        }
        
        public int Cantidad { get; }
        
        public List<GetOrdenModel> Ordenes { get; }
    }

}
using System;
using SISIFO2019.Model;
// ReSharper disable InconsistentNaming

namespace SISIFO2019.Mvc.Api.Ordenes.Models
{
    public class PostOrdenModel
    {
        public int Id { get; set; }
        
        public CRM CRM { get; set; }
        public string NumeroOrdenCRM { get; set; }
        public string NumeroEnlace { get; set; }
        
        public string ClienteRazonSocial { get; set; }
        public string ClienteCUIT { get; set; }
        
        public string NumeroOrdenSAP { get; set; }
        public int PMAsignadoId { get; set; }
        
        public string FechaInicio { get; set; }
        public int CompromisoEnDias { get; set; }
        public TipoDeOrden TipoOrden { get; set; }

        public string ProvinciaId { get; set; }
        public string DepartamentoId { get; set; }
        public string Direccion { get; set; }
        public string Comentarios { get; set; }
        
        public string UbicacionTecnica { get; set; }
        
        public string Nota { get; set; }
        
        public string TicketId { get; set; }
        
        public int? ResponsableInstFOId { get; set; }
    }
}
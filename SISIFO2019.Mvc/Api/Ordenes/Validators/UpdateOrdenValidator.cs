using System;
using FluentValidation;
using FluentValidation.AspNetCore;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Ordenes.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Ordenes.Validators
{
    public class UpdateOrdenValidator : AbstractValidator<UpdateOrdenModel>
    {
        // ReSharper disable once InconsistentNaming
        private const string NUMBER_REGEX = @"^\d+$";

        public UpdateOrdenValidator()
        {
            RuleFor(m => m.CRM)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .IsInEnum().WithMessage("Valor de enumeracion fuera de rango");

            RuleFor(o => o.NumeroOrdenCRM)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Matches(NUMBER_REGEX);
            RuleFor(o => o.NumeroOrdenCRM)
                .Length(8).WithMessage("Longitud esperada: 8")
                .When(o => o.CRM == CRM.Vantive);
            RuleFor(o => o.NumeroOrdenCRM)
                .Length(10).WithMessage("Longitud esperada: 10")
                .When(o => o.CRM == CRM.WorkflowClient);

            RuleFor(o => o.NumeroEnlace)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .MinimumLength(7).WithMessage("Longitud minima: 7")
                .MaximumLength(10).WithMessage("Longitud maxima: 10")
                .Matches(NUMBER_REGEX);

            RuleFor(o => o.RazonSocial)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .MinimumLength(3).WithMessage("Longitud mínima: 3")
                .MaximumLength(125).WithMessage("Longitud maxima: 125");

            RuleFor(o => o.CUIT)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Matches(@"^\d{2}-\d{8}-\d{1}$").WithMessage("Formato esperado: XX-XXXXXXXX-X");

            RuleFor(o => o.NumeroOrdenSAP)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Length(10).WithMessage("Longitud esperada: 10")
                .When(o => o.CRM == CRM.WorkflowClient);

//            RuleFor(o => o.PMAsignadoId)
//                .NotNull().WithMessage("Campo obligatorio")
//                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(o => o.TipoOrden)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .IsInEnum().WithMessage("Valor de enumeracion fuera de rango");

            RuleFor(o => o.ProvinciaId)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Length(2).WithMessage("Longitud esperada: 2");

            RuleFor(o => o.DepartamentoId)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Length(5).WithMessage("Longitud esperada: 5")
                .When(o => o.ProvinciaId != "02");

            RuleFor(o => o.Direccion)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .MinimumLength(5).WithMessage("Longitud mínima: 5")
                .MaximumLength(300).WithMessage("Longitud máxima: 300");


            RuleFor(o => o.FechaInicio)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Must(Utils.IsValidDateRepresentation).WithMessage("Formato de fecha invalido. Se esperaba DD/MM/AAAA");
            RuleFor(o => o.FechaInicio)
                .Must(f => Utils.ParseDateExact(f) <= DateTime.Now).WithMessage("Debe ser menor a hoy");


            RuleFor(o => o.Dias)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .GreaterThanOrEqualTo(20).WithMessage("Valor mínimo: 20")
                .LessThanOrEqualTo(365).WithMessage("Valor máximo: 365");


            RuleFor(o => o.FechaCierre)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Must(Utils.IsValidDateRepresentation).WithMessage("Formato de fecha invalido. Se esperaba DD/MM/AAAA")
                .Must(f => Utils.ParseDateExact(f) <= DateTime.Now).WithMessage("Debe ser menor a hoy")
                .When(o => o.Resultado != null);
            
            RuleFor(o => o.Resultado)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .When(o => !string.IsNullOrEmpty((o.FechaCierre)));

//            RuleFor(o => o.Comentarios)
//                .MaximumLength(1000).WithMessage("Longtid maxima: 1000")
//                .When(c => !string.IsNullOrEmpty(c.Comentarios));
        }
    }
}
using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Validators
{
    public class PostInstalacionValidator : AbstractValidator<PostInstalacionesModel>
    {
        public PostInstalacionValidator()
        {
            RuleFor(data => data.Cliente)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .MinimumLength(3).WithMessage("Longitud mínima: 3")
                .MaximumLength(125).WithMessage("Longitud maxima: 125");

            RuleFor(o => o.FechaInicio)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Must(Utils.IsValidDateRepresentation).WithMessage("Formato de fecha invalido. Se esperaba DD/MM/AAAA");

            When(i => Utils.IsValidDateRepresentation(i.FechaInicio), () =>
            {
                RuleFor(o => o.FechaInicio)
                    .Must(f => Utils.ParseDateExact(f) <= DateTime.Now).WithMessage("Debe ser menor a hoy");
            });


            RuleFor(o => o.FechaCompromiso)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio")
                .Must(Utils.IsValidDateRepresentation).WithMessage("Formato de fecha invalido. Se esperaba DD/MM/AAAA");
//            RuleFor(o => o.FechaCompromiso)
//                .Must(f => Utils.ParseDateExact(f) > DateTime.Now).WithMessage("Debe ser mayor a hoy");

            RuleFor(data => data.EstaPriorizada)
                .NotNull().WithMessage("Campo obligatorio");


            RuleFor(data => data.Estado)
                .NotNull().WithMessage("Campo obligatorio")
                .IsInEnum();

            RuleFor(data => data.Etapa)
                .NotNull().WithMessage("Campo obligatorio")
                .When(i => i.Estado != Estado.Terminada || i.Estado != Estado.Cancelada);

            RuleFor(data => data.Responsable)
                .NotNull().WithMessage("Campo obligatorio")
                .When(i => i.Estado != Estado.Terminada || i.Estado != Estado.Cancelada);
        }

    }
}
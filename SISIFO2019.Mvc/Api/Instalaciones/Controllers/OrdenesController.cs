using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Controllers
{
    [Route("api/instalaciones/{instalacionId:int}/ordenes")]
    [ApiController]
    public class OrdenesController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public OrdenesController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrdenCRM>>> Get(int instalacionId)
        {
            var result = await this._instalacionesContext.Ordenes
                .Where(o => o.InstalacionId == instalacionId)
                .ToListAsync();

            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<OrdenCRM>> Get(int instalacionId, int id)
        {
            var result = await this._instalacionesContext.Ordenes
                .Where(o => o.InstalacionId == instalacionId && o.Id == id)
                .ToListAsync();

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<OrdenCRM>> Post(int instalacionId, OrdenCRM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Ordenes)
                    .SingleOrDefaultAsync(i => i.Id == instalacionId);

                if (instalacion == null)
                {
                    return BadRequest($"No se encontro instalacion con id {instalacionId}");
                }

                this._instalacionesContext.Add(model);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    "Ordenes",
                    values: new {instalacionId, id = model.Id});
                

                return Created(url, model);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

       [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int instalacionId, int id)
        {
            try
            {
                var orden = await this._instalacionesContext.Ordenes
                    .SingleOrDefaultAsync(o => o.Id == id && o.InstalacionId == instalacionId);

                if (orden == null)
                {
                    return NotFound();
                }
                
                this._instalacionesContext.Remove(orden);
                await this._instalacionesContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
    
    
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Controllers
{
    [Route("api/instalaciones/{instalacionId:int}/interrupciones")]
    [ApiController]
    public class InterrupcionesController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public InterrupcionesController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Interrupcion>> Get(int instalacionId, int id)
        {
            var result = await this._instalacionesContext.Interrupciones
                .Where(i => i.InstalacionId == instalacionId && i.Id == id)
                .SingleOrDefaultAsync();

            if (result == null)
            {
                return NotFound();    
            }

            return Ok(new InterrupcionModel(result));
        }

        [HttpPost]
        public async Task<ActionResult<Interrupcion>> Post(int instalacionId, CreateInterrupcionModel createModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Interrupciones)
                    .SingleOrDefaultAsync(i => i.Id == instalacionId);

                if (instalacion == null)
                {
                    return BadRequest($"No se encontro instalacion con id {instalacionId}");
                }

                var interrupcion = new Interrupcion()
                {
                    InstalacionId = createModel.InstalacionId,
                    FechaInicio = Utils.ParseDateExact(createModel.FechaInicio),
                    FechaFin = Utils.ParseNullableDateExact(createModel.FechaFin),
                    Responsable = createModel.Responsable,
                    Comentarios = createModel.Comentarios,
                    EtapaInstalacion = createModel.EtapaInstalacion
                };


                // instalacion.Notas.Add(nota);

                var createdEntity = this._instalacionesContext.Add(interrupcion);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    values: new {instalacionId, id = createdEntity.Entity.Id});


                return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int instalacionId, int id)
        {
            try
            {
                var interrupcion = await this._instalacionesContext.Interrupciones
                    .SingleOrDefaultAsync(i => i.Id == id && i.InstalacionId == instalacionId);

                if (interrupcion == null)
                {
                    return NotFound();
                }

                this._instalacionesContext.Remove(interrupcion);
                await this._instalacionesContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
       [HttpPut("{id:int}")]
        public async Task<ActionResult<Interrupcion>> Put(int instalacionId, UpdateInterrupcionModel updateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var interrupcion = await this._instalacionesContext.Interrupciones
                    .SingleOrDefaultAsync(i => i.Id == updateModel.Id);

                if (interrupcion == null)
                {
                    return NotFound();
                }

                interrupcion.EtapaInstalacion = updateModel.EtapaInstalacion;
                interrupcion.FechaInicio = Utils.ParseDateExact(updateModel.FechaInicio);
                interrupcion.FechaFin = Utils.ParseNullableDateExact(updateModel.FechaFin);
                interrupcion.Responsable = updateModel.Responsable;
                interrupcion.Comentarios = updateModel.Comentarios;


                await _instalacionesContext.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
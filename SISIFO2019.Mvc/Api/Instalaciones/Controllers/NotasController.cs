﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Controllers
{
    [Route("api/instalaciones/{instalacionId:int}/notas")]
    [ApiController]
    public class NotasController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public NotasController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nota>>> Get(int instalacionId)
        {
            var result = await this._instalacionesContext.Notas
                .Include(n => n.Usuario)
                .Where(o => o.InstalacionId == instalacionId)
                .OrderByDescending(n => n.Fecha)
                .ThenByDescending(n => n.Id)
                .ToListAsync();

            return Ok(result);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Nota>> Get(int instalacionId, int id)
        {
            var result = await this._instalacionesContext.Notas
                .Include(n => n.Usuario)
                .Where(n => n.InstalacionId == instalacionId && n.Id == id)
                .SingleOrDefaultAsync();

            return Ok(result);
        }


        [HttpPost]
        public async Task<ActionResult<Nota>> Post(int instalacionId, CreateNotaModel createModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Notas)
                    .SingleOrDefaultAsync(i => i.Id == instalacionId);

                if (instalacion == null)
                {
                    return BadRequest($"No se encontro instalacion con id {instalacionId}");
                }

                var nota = new Nota()
                {
                    InstalacionId = instalacionId,
                    Contenido = createModel.Contenido,
                    Fecha = Utils.ParseDateExact(createModel.Fecha),
                    UsuarioId = createModel.UsuarioId
                };

                // instalacion.Notas.Add(nota);

                var createdEntity = this._instalacionesContext.Add(nota);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    values: new {instalacionId, id = createdEntity.Entity.Id});


                return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int instalacionId, int id)
        {
            try
            {
                var nota = await this._instalacionesContext.Notas
                    .SingleOrDefaultAsync(n => n.Id == id && n.InstalacionId == instalacionId);

                if (nota == null)
                {
                    return NotFound();
                }

                this._instalacionesContext.Remove(nota);
                await this._instalacionesContext.SaveChangesAsync();

                return Ok();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
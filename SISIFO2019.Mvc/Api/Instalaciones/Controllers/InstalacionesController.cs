using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Configuration.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.VisualBasic.CompilerServices;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Api.Ordenes.Models;
using Utils = SISIFO2019.Mvc.Validators.Utils;

namespace SISIFO2019.Mvc.Api.Instalaciones.Controllers
{
    [Route("api/instalaciones")]
    [ApiController]
    public class InstalacionesController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public InstalacionesController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<IActionResult> Get(bool incluirUbicacion = true)
        {
            try
            {
                var query = _instalacionesContext.Instalaciones
                    .OrderBy(i => i.Id)
                    .Include(i => i.Tareas)
                    .Include(i => i.UbicacionGeografica)
                    .Include(i => i.Ordenes)
                    .Include(i => i.PMAsignado)
                    .Include(i => i.Duenio)
                    .Include(i => i.Interrupciones);

                var instalaciones = await query.ToListAsync();
                var result = new GetInstalacionesModel(instalaciones);
                return Ok(result);
            }

            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            Instalacion instalacion = null;

            try
            {
                instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Ordenes)
                    .Include(i => i.Tareas)
                    .Include(i => i.UbicacionGeografica)
                    .Include(i => i.PMAsignado)
                    .Include(i => i.Duenio)
                    .Include(i => i.Interrupciones)
                    .SingleOrDefaultAsync(i => i.Id == id);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }


            if (instalacion == null)
            {
                return NotFound();
            }

            var result = new GetInstalacionModel(instalacion);
            return Ok(result);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .SingleOrDefaultAsync(i => i.Id == id);

                if (instalacion == null)
                {
                    return NotFound();
                }

                _instalacionesContext.Remove(instalacion);
                await _instalacionesContext.SaveChangesAsync();

                return this.StatusCode(StatusCodes.Status204NoContent);
            }
            catch (DbUpdateException e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.InnerException?.Message);
            }
        }
        

        [HttpPost]
        public async Task<ActionResult<GetInstalacionModel>> Post(PostInstalacionesModel createModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var instalacion = new Instalacion()
                {
                    Cliente = createModel.Cliente,
                    ClienteFinal = createModel.ClienteFinal,
                    FechaInicio = DateTime.ParseExact(createModel.FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    FechaCompromiso =
                        DateTime.ParseExact(createModel.FechaCompromiso, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    EstaPriorizada = createModel.EstaPriorizada,
                    Etapa = createModel.Etapa,
                    Estado = createModel.Estado,
                    Responsable = createModel.Responsable,
                    DuenioId = createModel.HipervisorId,
                    UbicacionGeograficaId = createModel.UbicacionGeograficaId,
                    Nota = createModel.Nota,
                    NombreDeProyecto = createModel.NombreDeProyecto,
                    Contratista = createModel.Contratista,
                    UbicacionTecnica = createModel.UbicacionTecnica,
                    PMAsignadoId = createModel.ResponsableMdDId,
                    ComentarioEstado = createModel.ComentarioEstado,
                    Tecnologia = createModel.Tecnologia,
                    
                };
                instalacion.DetallesUbicacion.Piso = createModel.Piso;
                instalacion.DetallesUbicacion.Comentarios = createModel.DetallesUbicacion;

               

                var posted = this._instalacionesContext.Add(instalacion);
                await _instalacionesContext.SaveChangesAsync();
                
                var orden = this._instalacionesContext.Ordenes.SingleOrDefault(o => o.Id == createModel.OrdenId);
                if (orden != null)
                {
                    orden.InstalacionId = posted.Entity.Id;
                }
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    "Instalaciones",
                    values: new {id = posted.Entity.Id});

                var created = _instalacionesContext.Instalaciones
                    .Include(i => i.UbicacionGeografica)
                    .Include(i => i.Ordenes)
                    .Include(i => i.Tareas)
                    .Include(i => i.Duenio)
                    .Include(i => i.PMAsignado)
                    .Single(i => i.Id == posted.Entity.Id);
                return Created(url, new GetInstalacionModel(created));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
       [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int id, PutInstalacionModel updateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Ordenes)
                    .Include(i => i.Tareas)
                    .Include(i => i.UbicacionGeografica)
                    .Include(i => i.PMAsignado)
                    .Include(i => i.Duenio)
                    .SingleOrDefaultAsync(i => i.Id == id);

                if (instalacion == null)
                {
                    return NotFound();
                }



                instalacion.Estado = updateModel.Estado;
                instalacion.Etapa = updateModel.Etapa;
                instalacion.Responsable = updateModel.Responsable;
                instalacion.Cliente = updateModel.Cliente;
                instalacion.ClienteFinal = updateModel.ClienteFinal;
                instalacion.FechaInicio = Utils.ParseDateExact(updateModel.FechaInicio);
                instalacion.FechaCompromiso = Utils.ParseDateExact(updateModel.FechaCompromiso);
                instalacion.UbicacionGeograficaId = updateModel.UbicacionId;
                instalacion.DuenioId = updateModel.HipervisorId;
                instalacion.DetallesUbicacion.Piso = updateModel.Piso;
                instalacion.DetallesUbicacion.Comentarios = updateModel.DetallesUbicacion;
                instalacion.Nota = updateModel.Nota;
                instalacion.NombreDeProyecto = updateModel.NombreDeProyecto;
                instalacion.Contratista = updateModel.Contratista;
                instalacion.UbicacionTecnica = updateModel.UbicacionTecnica;
                instalacion.PMAsignadoId = updateModel.ResponsableMdDId;
                instalacion.EstaPriorizada = updateModel.EstaPriorizada;
                instalacion.ComentarioEstado = updateModel.ComentarioEstado;

                instalacion.DetallesUbicacion.Latitud = updateModel.Latitud;
                instalacion.DetallesUbicacion.Longitud = updateModel.Longitud;
                instalacion.Tecnologia = updateModel.Tecnologia;
                instalacion.FechaTerminada = Utils.ParseNullableDateExact(updateModel.FechaTerminada);
                
                await _instalacionesContext.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        [HttpPut("{id:int}/terminar")]
         public async Task<ActionResult> Terminar(int id, TerminarModel updateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var instalacion = await this._instalacionesContext.Instalaciones
                    .Include(i => i.Ordenes)
                    .SingleOrDefaultAsync(i => i.Id == id);

                if (instalacion == null)
                {
                    return NotFound();
                }

                instalacion.Estado = Estado.Terminada;
                instalacion.Etapa = null;
                instalacion.Responsable = null;
                instalacion.FechaTerminada = Utils.ParseNullableDateExact(updateModel.FechaTerminada);

                if (updateModel.ActualizarOrdenesAsociadas)
                {
                    foreach (var orden in instalacion.Ordenes.Where(orden => orden.Cierre.Resultado == null && orden.Cierre.FechaCierre == null))
                    {
                        orden.Cierre.Resultado = ResultadoOrden.Exito;
                        orden.Cierre.FechaCierre = instalacion.FechaTerminada;
                    }
                }
                
                
                await _instalacionesContext.SaveChangesAsync();
                return NoContent();
                
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
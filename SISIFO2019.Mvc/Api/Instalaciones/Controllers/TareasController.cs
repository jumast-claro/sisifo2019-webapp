﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Mvc.Api.Instalaciones.Models.Tareas;
using SISIFO2019.Mvc.Api.Tareas.Models;

namespace SISIFO2019.Mvc.Api.Instalaciones.Controllers
{
    [Route("api/instalaciones/{instalacionId:int}/tareas")]
    [ApiController]
    public class TareasController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public TareasController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int instalacionId)
        {
            try
            {
                var tareas = await
                    _instalacionesContext.Tareas.Where(t => t.InstalacionId == instalacionId)
                        .Select(t => new TareaApiResponse(t))
                        .ToListAsync();
                return Ok(tareas);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
﻿using System;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class CreateInterrupcionModel
    {
        public int Id { get; set; }
        public int InstalacionId { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public ResponsableInterrupcion Responsable { get; set; }
        public string Comentarios { get; set; }
        
        public Etapa EtapaInstalacion { get; set; }
    }
}
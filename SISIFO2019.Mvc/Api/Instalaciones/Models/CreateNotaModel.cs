﻿namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class CreateNotaModel
    {
        public int InstalacionId { get; set; }
        public string Contenido { get; set; }
        public int UsuarioId { get; set; }
        
        public string Fecha { get; set; }
    }
}
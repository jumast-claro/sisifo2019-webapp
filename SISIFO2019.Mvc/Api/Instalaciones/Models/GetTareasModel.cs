using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetTareasModel
    {
        public GetTareasModel(List<Tarea> tareas)
        {
            Cantidad = tareas.Count;
            Tareas = tareas;
        }
        
        public int Cantidad { get; }
        
        public List<Tarea> Tareas { get; }
    }
}
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class PmAsignadoModel
    {
        public PmAsignadoModel(IntegranteMdD pmAsignado)
        {
            Id = pmAsignado.Id;
            Nombre = pmAsignado.Nombre;
            Apellido = pmAsignado.Apellido;
        }
        
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public string NombreCompleto
        {
            get { return Nombre + " " + Apellido; }
        }
    }
}
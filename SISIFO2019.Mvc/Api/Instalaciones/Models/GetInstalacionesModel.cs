using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")] 
    public class GetInstalacionesModel
    {
        public GetInstalacionesModel(List<Instalacion> instalaciones)
        {
            Cantidad = instalaciones.Count();
            Instalaciones = instalaciones.Select(i => new GetInstalacionModel(i)).ToList();
        }
        
        public int Cantidad { get; }

        public List<GetInstalacionModel> Instalaciones { get; }
    }
}
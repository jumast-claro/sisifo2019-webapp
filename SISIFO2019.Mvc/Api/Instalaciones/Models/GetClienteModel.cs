using System.Diagnostics.CodeAnalysis;
using System.Dynamic;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetClienteModel
    {
        public GetClienteModel(string razonSocial, string clienteFinal)
        {
            RazonSocial = razonSocial;
            ClienteFinal = clienteFinal;
        }

        public string RazonSocial { get; }

        public string ClienteFinal { get; }
        
    }
}
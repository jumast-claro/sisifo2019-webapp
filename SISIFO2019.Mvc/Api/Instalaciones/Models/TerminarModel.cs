﻿namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class TerminarModel
    {
        public string FechaTerminada { get; set; }
        public bool ActualizarOrdenesAsociadas { get; set; }
    }
}
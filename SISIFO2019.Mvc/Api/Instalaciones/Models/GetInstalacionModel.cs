using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;
using ordenes = SISIFO2019.Mvc.Api.Ordenes.Models;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetInstalacionModel
    {
        public GetInstalacionModel(Instalacion instalacion)
        {
            Id = instalacion.Id;
            Cliente = new GetClienteModel(instalacion.Cliente, instalacion.ClienteFinal);
            Ubicacion = new GetUbicacionModel(instalacion.UbicacionGeografica);
            Seguimiento = new GetInfoSeguimientoModel(instalacion.Estado, instalacion.Etapa, instalacion.Responsable);

            Ordenes = new ordenes.GetOrdenesModel(instalacion.Ordenes);
            Tareas = new GetTareasModel(instalacion.Tareas);
            Compromiso = new CompromisoModel(instalacion.FechaCompromiso);

            if (instalacion.PMAsignado != null)
            {
                PmAsignado = new PmAsignadoModel(instalacion.PMAsignado);
            }

            if (instalacion.Duenio != null)
            {
                this.Duenio = new IntegranteInstalacionesFOModel(instalacion.Duenio);
            }

            FechaInicio = Utils.DateToString(instalacion.FechaInicio);

            DetallesUbicacion = instalacion.DetallesUbicacion;
            UbicacionTecnica = instalacion.UbicacionTecnica;
            ResponsableMdD = instalacion.PMAsignado;
            Contratista = instalacion.Contratista;
            ComentarioEstado = instalacion.ComentarioEstado;

            Latitud = instalacion.DetallesUbicacion.Latitud;
            Longitud = instalacion.DetallesUbicacion.Longitud;
            Piso = instalacion.DetallesUbicacion.Piso;

            Tecnologia = new EnumModel<Tecnologia>(instalacion.Tecnologia);
            FechaTerminada = instalacion.FechaTerminada.HasValue ? Utils.DateToString(instalacion.FechaTerminada.Value) : "";

            OnTimeCrudo = instalacion.OnTimeCrudo;
            TiempoBrutoInstalacionEnDias = instalacion.TiempoBrutoInstalacionEnDias;
            TiempoNetoEnDias = instalacion.TiempoNetoEnDias;
            TiempoEnPermisosEnDias = instalacion.TiempoEnPermisosEnDias;
            TiempoInterrumpidaEnDias = instalacion.TiempoInterrumpidaEnDias;

            this.Interrupciones = instalacion.Interrupciones.Select(i => new InterrupcionModel(i)).ToList();
        }

        public int Id { get; set; }
        public GetClienteModel Cliente { get; }

        public GetUbicacionModel Ubicacion { get; }

        public ordenes.GetOrdenesModel Ordenes { get; }

        public GetInfoSeguimientoModel Seguimiento { get; }

        public GetTareasModel Tareas { get; }
        
        public CompromisoModel Compromiso { get; }
        
        public PmAsignadoModel PmAsignado { get; set; }
        
        public IntegranteInstalacionesFOModel Duenio { get; set; }
        
        public string FechaInicio { get; set; }

        public DetallesUbicacion DetallesUbicacion { get; set; }
        
        public string UbicacionTecnica { get; set; }
        public IntegranteMdD ResponsableMdD { get; set; }
        public Contratista? Contratista { get; set; }
        public string ComentarioEstado { get; set; }
        
        public double? Latitud { get; set; }
        public double? Longitud { get; set; }
        public int? Piso { get; set; }
        
        public EnumModel<Tecnologia> Tecnologia { get; }
        
        public string FechaTerminada { get; set; }
        
        public bool? OnTimeCrudo { get; set; }
        
        public int TiempoBrutoInstalacionEnDias { get; }
        
        public int TiempoNetoEnDias { get; }
        
            public int TiempoEnPermisosEnDias { get; }
            
            public int TiempoInterrumpidaEnDias { get; }

        public List<InterrupcionModel> Interrupciones { get; set; }
        
    }
}
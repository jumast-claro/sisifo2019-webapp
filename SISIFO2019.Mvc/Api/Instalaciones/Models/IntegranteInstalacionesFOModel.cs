using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class IntegranteInstalacionesFOModel
    {
        public IntegranteInstalacionesFOModel(IntegranteInstFO integranteInstFo)
        {
            this.Id = integranteInstFo.Id;
            this.Nombre = integranteInstFo.Nombre;
            this.Apellido = integranteInstFo.Apellido;
        }
        
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NombreCompleto
        {
            get { return Nombre + " " + Apellido; }
        }
        
    }
}
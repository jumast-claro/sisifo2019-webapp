using System;
using System.Diagnostics.CodeAnalysis;
using AutoMapper.Configuration.Annotations;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class EnumModel<T> where T : Enum
    {
        private readonly T _valor;

        public EnumModel(T valor)
        {
            _valor = valor;
        }

        public T Valor => this._valor;

        public string Nombre => this._valor.ToString();
    }

}
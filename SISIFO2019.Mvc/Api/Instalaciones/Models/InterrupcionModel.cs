﻿using Microsoft.Win32;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class InterrupcionModel
    {
        public InterrupcionModel(Interrupcion interrupcion)
        {
            this.Id = interrupcion.Id;
            this.InstalacionId = interrupcion.InstalacionId;
            this.FechaInicio = Utils.DateToString(interrupcion.FechaInicio);
            this.FechaFin = Utils.DateToString(interrupcion.FechaFin);
            this.Responsable = interrupcion.Responsable;
            this.Comentarios = interrupcion.Comentarios;
            this.DuracionEnDias = interrupcion.DuracionEnDias;
            this.EtapaInstalacion = interrupcion.EtapaInstalacion;
        }

        public int Id { get; set; }
        public int InstalacionId { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public ResponsableInterrupcion Responsable { get; set; }
        public string Comentarios { get; set; }
        
        public int DuracionEnDias { get; }
        
        public Etapa EtapaInstalacion { get; }
    }
}
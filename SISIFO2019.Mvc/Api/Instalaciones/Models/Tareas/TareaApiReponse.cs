﻿using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models.Tareas
{
    public class TareaApiResponse
    {
        public TareaApiResponse(Tarea tarea)
        {
            Id = tarea.Id;
            InstalacionId = tarea.InstalacionId;
            FechaInicio = Utils.DateTimeToString(tarea.FechaInicio);
            TipoTarea = tarea.Tipo;
            Contratista = tarea.Contratista;
            EstadoTarea = tarea.Estado;
            Descripcion = tarea.Descripcion;

            RequiereCoordinacion = tarea.RequiereCoordinacion;
            EstadoCoordinacion = tarea.EstadoCoordinacion;
            ResponsableCoordinacion = tarea.ResponsableCoordinacion;
            ComentarioCoordinacion = tarea.ComentarioCoordinacion;
            DuracionEnHoras = tarea.DuracionEnHoras;
            TecnicoResponsable = tarea.TecnicoResponsable;
            ComentarioEstado = tarea.ComentarioEstado;

        }

        public int Id { get; }
        public int InstalacionId { get; }
        public string FechaInicio { get; }
        public TipoDeTarea TipoTarea { get; }
        public Contratista Contratista { get; }
        public EstadoDeTarea EstadoTarea { get; }
        public string Descripcion { get; }


        public bool RequiereCoordinacion { get; set; }
        public EstadoCoordinacion? EstadoCoordinacion { get; set; }
        public Responsable? ResponsableCoordinacion { get; set; }
        public string ComentarioCoordinacion { get; set; }

        public int DuracionEnHoras { get; set; }
        public string TecnicoResponsable { get; set; }

        public string ComentarioEstado { get; set; }
        
        
    }
}
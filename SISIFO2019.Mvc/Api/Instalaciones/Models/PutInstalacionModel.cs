using System;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public class PutInstalacionModel : IInstalacionModel
    {
        public Etapa? Etapa { get; set; }
        public Estado Estado { get; set; }
        public Responsable? Responsable { get; set; }
        
        public string Cliente { get; set; }
        
        public string ClienteFinal { get; set; }

        public string FechaInicio { get; set; }
        
        public string FechaCompromiso { get; set; }
        
        public int? UbicacionId { get; set; }
        
        public int? HipervisorId { get; set; }
        
        public bool EstaPriorizada { get; set; }
        
        public int? Piso { get; set; }
        
        public string DetallesUbicacion { get; set; }
        
        public string Nota { get; set; }
        
        public int? ResponsableMdDId { get; set; }
                
        public Contratista? Contratista { get; set; }
                
        public string UbicacionTecnica { get; set; }
                
        public string NombreDeProyecto { get; set; }
        
         public string ComentarioEstado { get; set; }
         
         public double? Latitud { get; set; } 
         public double? Longitud { get; set; }
         public Tecnologia Tecnologia { get; set; }
         public string FechaTerminada { get; set; }
    }
}
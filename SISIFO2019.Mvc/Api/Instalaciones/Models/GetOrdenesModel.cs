using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetOrdenesModel
    {
        public GetOrdenesModel(List<OrdenCRM> ordenes)
        {
            Cantidad = ordenes.Count;
            Ordenes = ordenes;
        }
        
        public int Cantidad { get; }
        
        public List<OrdenCRM> Ordenes { get; }
    }

   
}
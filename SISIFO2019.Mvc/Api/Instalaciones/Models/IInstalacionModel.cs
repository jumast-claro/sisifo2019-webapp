﻿using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    public interface IInstalacionModel
    {
        Etapa? Etapa { get; set; }
        Estado Estado { get; set; }
        Responsable? Responsable { get; set; }
        string Cliente { get; set; }
        
        string ClienteFinal { get; set; }

        string FechaInicio { get; set; }
        
        string FechaCompromiso { get; set; }
        
        int? UbicacionId { get; set; }
        
        int? HipervisorId { get; set; }
        
        bool EstaPriorizada { get; set; }
    }
}
using System.Diagnostics.CodeAnalysis;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api._Shared.Models;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetUbicacionModel
    {
        public GetUbicacionModel(UbicacionGeografica ubicacion)
        {
            Id = ubicacion.Id;
//            InstalacionId = ubicacion.InstalacionId;
            Provincia = new DatoGeograficoModel(ubicacion.ProvinciaId, ubicacion.ProvinciaNombre);
            Departamento = new DatoGeograficoModel(ubicacion.DepartamentoId, ubicacion.DepartamentoNombre);
            Municipio = new DatoGeograficoModel(ubicacion.MunicipioId, ubicacion.MunicipioNombre);
            Localidad = new DatoGeograficoModel(ubicacion.LocalidadId, ubicacion.LocalidadNombre);
            Calle = new DatoGeograficoModel(ubicacion.CalleId, ubicacion.CalleNombre);
            Coordenadas = new CoordenadasModel(ubicacion.Latitud, ubicacion.Longitud);
            Numero = ubicacion.Altura;
            Direccion = ubicacion.Direccion;
//            Piso = ubicacion.Piso;
        }

        public int Id { get; }

        public int InstalacionId { get; }

        public DatoGeograficoModel Provincia { get; }

        public DatoGeograficoModel Departamento { get; }

        public DatoGeograficoModel Municipio { get; }

        public DatoGeograficoModel Localidad { get; }

        public DatoGeograficoModel Calle { get; }
        
        public CoordenadasModel Coordenadas { get; }

        public string Numero { get; }
        
        public string Direccion {get;}

//        public int? Piso { get; }
    }
}
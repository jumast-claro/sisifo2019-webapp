using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class CompromisoModel
    {
        public CompromisoModel(DateTime fechaCompromiso)
        {
            Fecha = fechaCompromiso;
        }

        public DateTime Fecha { get; }

        public int DiasHastaVencimiento => (Fecha - DateTime.Today).Days;

        public bool Vencido => Fecha < DateTime.Today;

        public string NombreDelDia => DateTimeFormatInfo.CurrentInfo.GetDayName(Fecha.DayOfWeek).ToUpper();

        public string FormatedDate => Utils.DateToString(Fecha);
    }
    
}
using System.Diagnostics.CodeAnalysis;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Api.Instalaciones.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class GetInfoSeguimientoModel
    {
        public GetInfoSeguimientoModel(Estado estado, Etapa? etapa, Responsable? responsable)
        {
            Estado = new EnumModel<Estado>(estado);
            Etapa = etapa.HasValue ? new EnumModel<Etapa>(etapa.Value) : null;
            Responsable = responsable.HasValue ? new EnumModel<Responsable>(responsable.Value) : null;
        }


        public EnumModel<Estado> Estado { get; }

        public EnumModel<Etapa> Etapa { get; }

        public EnumModel<Responsable> Responsable { get; }
    }
}
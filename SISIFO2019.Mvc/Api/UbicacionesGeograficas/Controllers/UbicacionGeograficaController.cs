using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Ordenes.Models;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Mappers;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Controllers
{
    [Route("api/ubicaciones")]
    [ApiController]
    public class UbicacionGeograficaController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly DatosGeograficosContext _datosGeograficosContext;
        private readonly LinkGenerator _linkGenerator;

        public UbicacionGeograficaController(InstalacionesContext instalacionesContext,
            DatosGeograficosContext datosGeograficosContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _datosGeograficosContext = datosGeograficosContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<UbicacionGeografica>> Get()
        {
            try
            {
                var ubicacionesGeografica = await this._instalacionesContext.UbicacionesGeograficas.Include(u => u.Instalaciones).ThenInclude(i => i.Duenio).ToListAsync();
                var mapper = new GetMapper();
                return Ok(ubicacionesGeografica.Select(u => mapper.Map(u)));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [HttpGet("{id:int}")]
        public async Task<ActionResult<UbicacionGeografica>> Get(int id)
        {
            try
            {
                var ubicacionGeografica = await this._instalacionesContext.UbicacionesGeograficas
                .Include(s => s.Instalaciones)
                   .Where(o => o.Id == id)
                    .SingleOrDefaultAsync();
                
                if (ubicacionGeografica == null)
                {
                    return NotFound();
                }

                var mapper = new GetMapper();
                return Ok(mapper.Map(ubicacionGeografica));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }


        [HttpPost]
        public async Task<ActionResult<UbicacionGeografica>> Post(PostUbicacionGeograficaModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var mapper = new PostMapper(_datosGeograficosContext);
                var ubicacionGeografica = mapper.Mapp(model);

                var posted = this._instalacionesContext.Add(ubicacionGeografica);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    "UbicacionGeografica",
                    values: new {id = posted.Entity.Id});

                var created = _instalacionesContext.UbicacionesGeograficas.Single(o => o.Id == posted.Entity.Id);
                
                var getMapper = new GetMapper();
                return Created(url, getMapper.Map(created));
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
        
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var sitio = await this._instalacionesContext.UbicacionesGeograficas
                    .SingleOrDefaultAsync(s => s.Id == id);

                if (sitio == null)
                {
                    return NotFound();
                }

                _instalacionesContext.Remove(sitio);
                await _instalacionesContext.SaveChangesAsync();

                return this.StatusCode(StatusCodes.Status204NoContent);
            }
            catch (DbUpdateException e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.InnerException?.Message);
            }
        }
    }
}
using FluentValidation;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Validators
{
    public class PostUbicacionGeograficaValidator : AbstractValidator<PostUbicacionGeograficaModel>
    {
        public PostUbicacionGeograficaValidator()
        {
            RuleFor(u => u.Latitud)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(u => u.Longitud)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(u => u.ProvinciaId)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(u => u.DepartamentoId)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(u => u.Direccion)
                .NotNull().WithMessage("Campo obligatorio")
                .NotEmpty().WithMessage("Campo obligatorio");

            RuleFor(u => u.CodigoPostal)
                .MinimumLength(4).WithMessage("Longitud minima: 4")
                .MaximumLength(8).WithMessage("Longitud maxima: 8");

            RuleFor(u => u.Comentarios)
                .MaximumLength(2500).WithMessage("Longitud maxima 2500");
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api._Shared.Models;
using SISIFO2019.Mvc.Api.Instalaciones.Controllers;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Mappers
{
    public class GetMapper
    {

        public GetMapper()
        {
        }

        public GetModel Map(UbicacionGeografica entity)
        {
            var model = new GetModel()
            {
                Id = entity.Id,
                Coordenadas = new CoordenadasModel(entity.Latitud, entity.Longitud),
                Provincia = new DatoGeograficoModel(entity.ProvinciaId, entity.ProvinciaNombre),
                Departamento = new DatoGeograficoModel(entity.DepartamentoId, entity.DepartamentoNombre),
                Municipio = new DatoGeograficoModel(entity.MunicipioId, entity.MunicipioNombre),
                Localidad = new DatoGeograficoModel(entity.LocalidadId, entity.LocalidadNombre),
                Calle = new DatoGeograficoModel(entity.CalleId, entity.CalleNombre),
                Altura = entity.Altura,
                CodigoPostal = entity.CodigoPostal,
                Direccion = entity.Direccion,
                Nombre = entity.Nombre,
                IdentificadorCatastral = entity.IdentificadorCatastral,
                
            };

            if (entity.Instalaciones != null)
            {
                model.Instalaciones = entity.Instalaciones.Select(i => new InstalacionModel(i));
            }
            else
            {
                model.Instalaciones = new List<InstalacionModel>();
            }

            return model;
        }
    }
}
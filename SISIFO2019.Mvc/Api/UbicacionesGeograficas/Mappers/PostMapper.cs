using System.Linq;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Mappers
{
    public class PostMapper
    {
        private readonly DatosGeograficosContext _datosGeograficosContext;

        public PostMapper(DatosGeograficosContext datosGeograficosContext)
        {
            _datosGeograficosContext = datosGeograficosContext;
        }

        public UbicacionGeografica Mapp(PostUbicacionGeograficaModel model)
        {
            var ubicacionGeografica = new UbicacionGeografica()
            {
                Latitud = model.Latitud,
                Longitud = model.Longitud,
                ProvinciaId = model.ProvinciaId,
                ProvinciaNombre = _datosGeograficosContext.Provincias.Single(p => p.Id == model.ProvinciaId).Nombre,
                DepartamentoId = model.DepartamentoId,
                DepartamentoNombre = this._datosGeograficosContext.Departamentos
                    .Single(d => d.Id == model.DepartamentoId).Nombre,
                MunicipioId = model.MunicipioId,
                MunicipioNombre = this._datosGeograficosContext.Municipios
                    .SingleOrDefault(m => m.Id == model.MunicipioId)?
                    .Nombre,
                Direccion = model.Direccion,
                CalleId = model.CalleId,
                CalleNombre = _datosGeograficosContext.Calles.SingleOrDefault(c => c.Id == model.CalleId)?.Nombre,
                Altura = model.Altura,
                LocalidadId = model.LocalidadId,
                LocalidadNombre = _datosGeograficosContext.Localidades.SingleOrDefault(l => l.Id == model.LocalidadId)
                    ?.Nombre,
                CodigoPostal = model.CodigoPostal,
                Comentarios = model.Comentarios,
                
                Nombre = model.Nombre,
                IdentificadorCatastral = model.IdentificadorCatastral,
                    
            };

            return ubicacionGeografica;
        }
    }
}
namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models
{
    public class PostUbicacionGeograficaModel
    {
        public int Id { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string ProvinciaId { get; set; }
        public string DepartamentoId { get; set; }
        public string MunicipioId { get; set; }
        public string Direccion { get; set; }
        public string CalleId { get; set; }
        public string Altura { get; set; }
        public string LocalidadId { get; set; }
        public string CodigoPostal { get; set; }
        public string Comentarios { get; set; }
        
        public string Nombre { get; set; }
        
        public string IdentificadorCatastral { get; set; }
    }
}
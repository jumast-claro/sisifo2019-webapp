using System.Collections.Generic;
using SISIFO2019.Mvc.Api._Shared.Models;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models
{
    public class GetModel
    {
       public int Id { get; set; } 
       public CoordenadasModel  Coordenadas { get; set; }
       public DatoGeograficoModel Provincia { get; set; }
       public DatoGeograficoModel Departamento { get; set; }
       public DatoGeograficoModel Municipio { get; set; }
       public DatoGeograficoModel Localidad { get; set; }
       public DatoGeograficoModel Calle { get; set; }
       public string Altura { get; set; }
       public string CodigoPostal { get; set; }
       public string Direccion { get; set; }
       public string Comentarios { get; set; }
       
       public string Nombre { get; set; }

       public string IdentificadorCatastral { get; set; }

       public IEnumerable<InstalacionModel> Instalaciones { get; set; }
    }
}
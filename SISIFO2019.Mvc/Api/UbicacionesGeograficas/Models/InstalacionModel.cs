using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.UbicacionesGeograficas.Models
{
    public class InstalacionModel
    {

        public InstalacionModel(Instalacion instalacion)
        {
            
            this.Id = instalacion.Id;
            this.Cliente = instalacion.Cliente;
            this.Etapa = instalacion.Etapa.ToString();
            this.Estado = instalacion.Estado.ToString();
            this.Responsable = instalacion.Responsable.ToString();
            this.Piso = instalacion.DetallesUbicacion.Piso.ToString();
            this.ReponsableInstFO = instalacion.Duenio != null ? instalacion.Duenio.NombreCompleto : "";
            this.FechaInicio = Utils.DateToString(instalacion.FechaInicio);
            this.FechaCompromiso = Utils.DateToString(instalacion.FechaCompromiso);
            this.UbicacionTecnica = instalacion.UbicacionTecnica;
            this.ComentarioEstado = instalacion.ComentarioEstado;


        }
        
        public int Id { get; set; }
        public string Cliente { get; set; }
        
        public string Estado { get; }
        
        public string Etapa { get; set; }
        
        public string Responsable { get; set; }
        
        public string Piso { get; }
        
        public string ReponsableInstFO { get; }
        
        
        public string FechaInicio { get; }
        
        public string FechaCompromiso { get; }
        
        public string UbicacionTecnica { get; set; }
        
        public string ComentarioEstado { get; }
        
    }
}
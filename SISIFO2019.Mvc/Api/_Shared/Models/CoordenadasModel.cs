using System.Diagnostics.CodeAnalysis;

namespace SISIFO2019.Mvc.Api._Shared.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class CoordenadasModel
    {
        public CoordenadasModel(double? latitud, double? longitud)
        {
            Latitud = latitud;
            Longitud = longitud;
        }

        public double? Latitud { get; }
        public double? Longitud { get; }
    }
}
using System.Diagnostics.CodeAnalysis;

namespace SISIFO2019.Mvc.Api._Shared.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DatoGeograficoModel
    {
        public DatoGeograficoModel(string id, string nombre)
        {
            Id = id;
            Nombre = nombre;
        }

        public string Id { get; }
        public string Nombre { get; }
    }
}
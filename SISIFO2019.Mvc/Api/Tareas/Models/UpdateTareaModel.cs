﻿using SISIFO2019.Model;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Tareas.Models
{
    public class UpdateTareaModel
    {

        public UpdateTareaModel(Tarea tarea)
        {
            InstalacionId = tarea.InstalacionId;
            FechaInicio = Utils.DateTimeToString(tarea.FechaInicio);
            TipoTarea = tarea.Tipo;
            Contratista = tarea.Contratista;
            EstadoTarea = tarea.Estado;
            Descripcion = tarea.Descripcion;
        }

        public UpdateTareaModel()
        {
            
        }
        
        public int InstalacionId { get; set; }
        public string FechaInicio { get; set; }
        public TipoDeTarea TipoTarea { get; set; }
        public Contratista Contratista { get; set; }
        public EstadoDeTarea EstadoTarea { get; set; } 
        public string Descripcion { get; set; }
        
        
        public bool RequiereCoordinacion { get; set; }
        public EstadoCoordinacion? EstadoCoordinacion { get; set; }
        public Responsable? ResponsableCoordinacion { get; set; }
        public string ComentarioCoordinacion { get; set; }
        public int DuracionEnHoras { get; set; }
        public string TecnicoResponsable { get; set; }
        public string ComentarioEstado { get; set; }
    }
}
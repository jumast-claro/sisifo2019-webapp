﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Api.Instalaciones.Models;
using SISIFO2019.Mvc.Api.Tareas.Models;
using SISIFO2019.Mvc.Validators;

namespace SISIFO2019.Mvc.Api.Tareas.Controllers
{
    [Route("api/tareas")]
    [ApiController]
    public class TareasController : ControllerBase
    {
        private readonly InstalacionesContext _instalacionesContext;
        private readonly LinkGenerator _linkGenerator;

        public TareasController(InstalacionesContext instalacionesContext, LinkGenerator linkGenerator)
        {
            _instalacionesContext = instalacionesContext;
            _linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var tareas = await
                    _instalacionesContext.Tareas
                                 .Include(t => t.Instalacion).ThenInclude(i => i.Interrupciones)
                        .Include(t => t.Instalacion).ThenInclude(i => i.UbicacionGeografica)
                        .Select(t => new TareaModel(t))
                        .ToListAsync();
                return Ok(tareas);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            Tarea tarea = null;
            try
            {
                tarea = await _instalacionesContext.Tareas
                    .Include(t => t.Instalacion).ThenInclude(i => i.Interrupciones)
                    .Include(t => t.Instalacion).ThenInclude(i => i.UbicacionGeografica)
                    .SingleOrDefaultAsync(t => t.Id == id);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }

            if (tarea == null)
            {
                return NotFound();
            }

            return Ok(new TareaModel(tarea));
        }


        [HttpPost]
        public async Task<ActionResult<Tarea>> Post(CreateTareaModel createModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var tarea = new Tarea
                {
                    InstalacionId = createModel.InstalacionId,
                    FechaInicio = Utils.ParseNullableDateTimeExact(createModel.FechaInicio),
                    Tipo = createModel.TipoTarea,
                    Contratista = createModel.Contratista,
                    Estado = createModel.EstadoTarea,
                    Descripcion = createModel.Descripcion,
                    
                    RequiereCoordinacion = createModel.RequiereCoordinacion,
                    EstadoCoordinacion = createModel.EstadoCoordinacion,
                    ResponsableCoordinacion = createModel.ResponsableCoordinacion,
                    ComentarioCoordinacion = createModel.ComentarioCoordinacion,
                    DuracionEnHoras = createModel.DuracionEnHoras,
                    TecnicoResponsable = createModel.TecnicoResponsable,
                    ComentarioEstado = createModel.ComentarioEstado
                    
                };

                var posted = this._instalacionesContext.Add(tarea);
                await _instalacionesContext.SaveChangesAsync();

                var url = _linkGenerator.GetPathByAction(
                    HttpContext,
                    "Get",
                    "Tareas",
                    values: new {id = posted.Entity.Id});

                var created = _instalacionesContext.Tareas.Single(t => t.Id == posted.Entity.Id);

                return Created(url, created);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var tarea = await this._instalacionesContext.Tareas
                    .SingleOrDefaultAsync(i => i.Id == id);

                if (tarea == null)
                {
                    return NotFound();
                }

                _instalacionesContext.Remove(tarea);
                await _instalacionesContext.SaveChangesAsync();

                return this.StatusCode(StatusCodes.Status204NoContent);
            }
            catch (DbUpdateException e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, e.InnerException?.Message);
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int id, UpdateTareaModel updateTareaModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var tarea = await this._instalacionesContext.Tareas.SingleOrDefaultAsync(i => i.Id == id);
                if (tarea == null)
                {
                    return NotFound();
                }

                tarea.FechaInicio = Utils.ParseNullableDateTimeExact(updateTareaModel.FechaInicio);
                tarea.Tipo = updateTareaModel.TipoTarea;
                tarea.Contratista = updateTareaModel.Contratista;
                tarea.Estado = updateTareaModel.EstadoTarea;
                tarea.Descripcion = updateTareaModel.Descripcion;

                tarea.RequiereCoordinacion = updateTareaModel.RequiereCoordinacion;
                tarea.EstadoCoordinacion = updateTareaModel.EstadoCoordinacion;
                tarea.ResponsableCoordinacion = updateTareaModel.ResponsableCoordinacion;
                tarea.ComentarioCoordinacion = updateTareaModel.ComentarioCoordinacion;
                tarea.DuracionEnHoras = updateTareaModel.DuracionEnHoras;
                tarea.TecnicoResponsable = updateTareaModel.TecnicoResponsable;
                tarea.ComentarioEstado = updateTareaModel.ComentarioEstado;
                    
                await _instalacionesContext.SaveChangesAsync();
                return NoContent();
            }

            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }
    }
}
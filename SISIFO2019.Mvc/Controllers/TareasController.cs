﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;
using SISIFO2019.Mvc.Models.Tareas;

namespace SISIFO2019.Mvc.Controllers
{
    public class TareasController : Controller
    {
        private readonly InstalacionesContext _context;

        public TareasController(InstalacionesContext context)
        {
            _context = context;
        }

        // GET: Tareas
        public async Task<IActionResult> Index()
        {
            return View("TareasAgrupadasPorFecha", await _context.Tareas
                .OrderBy(t => t.FechaInicio)
                .Select(tarea => new TareaViewModel(tarea))
                .ToListAsync());
        }

        // GET: Tareas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarea = await _context.Tareas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tarea == null)
            {
                return NotFound();
            }

            return View(tarea);
        }

        // GET: Tareas/Create
        public IActionResult Create(int? instalacionId)
        {
            if (instalacionId == null)
            {
                return View();
            }
            return View(new CrearTareaViewModel(){InstalacionId = instalacionId.Value});
        }

        // POST: Tareas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,InstalacionId,FechaInicio,FechaFin,Tipo,Contratista,Descripcion,Estado,DuracionHoras,DuracionMinutos")] CrearTareaViewModel crearTareaViewModel)
        {
            if (ModelState.IsValid)
            {

                var fechaInicio = crearTareaViewModel.FechaInicio;

                var fechaFin = fechaInicio.Value;
                fechaFin = fechaFin.AddHours(crearTareaViewModel.DuracionHoras);
                fechaFin = fechaFin.AddMinutes(crearTareaViewModel.DuracionMinutos);

                var tarea = new Tarea()
                {
                    Id = crearTareaViewModel.Id,
                    InstalacionId = crearTareaViewModel.InstalacionId,
                    FechaInicio = crearTareaViewModel.FechaInicio,
                    FechaFin = fechaFin,
                    Tipo = crearTareaViewModel.Tipo,
                    Contratista = crearTareaViewModel.Contratista,
                    Descripcion = crearTareaViewModel.Descripcion,
                    Estado = crearTareaViewModel.Estado
                };

                //var tarea = _mapper.Map<Tarea>(crearTareaViewModel);
                _context.Add(tarea);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            crearTareaViewModel.InstalacionId = 0;
            return View(crearTareaViewModel);
        }

        // GET: Tareas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarea = await _context.Tareas.FindAsync(id);
            if (tarea == null)
            {
                return NotFound();
            }
            return View(tarea);
        }

        // POST: Tareas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,InstalacionId,FechaInicio,FechaFin,Tipo,Contratista,Descripcion,Estado")] Tarea tarea)
        {
            if (id != tarea.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tarea);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TareaExists(tarea.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tarea);
        }

        // GET: Tareas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tarea = await _context.Tareas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tarea == null)
            {
                return NotFound();
            }

            return View(tarea);
        }

        // POST: Tareas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tarea = await _context.Tareas.FindAsync(id);
            _context.Tareas.Remove(tarea);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TareaExists(int id)
        {
            return _context.Tareas.Any(e => e.Id == id);
        }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SISIFO2019.Data;
using SISIFO2019.Model;

namespace SISIFO2019.Mvc.Controllers
{
    public class OrdenesController : Controller
    {
        private readonly InstalacionesContext _context;

        public OrdenesController(InstalacionesContext context)
        {
            _context = context;
        }

        // GET: Ordenes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Ordenes
                .OrderBy(o => o.Cliente)
                .ToListAsync());
        }

        // GET: Ordenes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ordenCRM = await _context.Ordenes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ordenCRM == null)
            {
                return NotFound();
            }

            return View(ordenCRM);
        }

        // GET: Ordenes/Create
        public IActionResult Create(int? instalacionId = null)
        {

            if (instalacionId == null)
            {
                return View();
            }

            return View(new OrdenCRM(){InstalacionId = instalacionId});
        }

        // POST: Ordenes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,InstalacionId,Cliente,Direccion,Partido,NumeroOrden,NumeroEnlace,FechaInicio,FechaCierre,Tipo,CRM")] OrdenCRM ordenCRM)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ordenCRM);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ordenCRM);
        }

        // GET: Ordenes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ordenCRM = await _context.Ordenes.FindAsync(id);
            if (ordenCRM == null)
            {
                return NotFound();
            }
            return View(ordenCRM);
        }

        // POST: Ordenes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,InstalacionId,Cliente,Direccion,Partido,NumeroOrden,NumeroEnlace,FechaInicio,FechaCierre,Tipo,CRM")] OrdenCRM ordenCRM)
        {
            if (id != ordenCRM.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ordenCRM);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrdenCRMExists(ordenCRM.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ordenCRM);
        }

        // GET: Ordenes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ordenCRM = await _context.Ordenes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ordenCRM == null)
            {
                return NotFound();
            }

            return View(ordenCRM);
        }

        // POST: Ordenes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ordenCRM = await _context.Ordenes.FindAsync(id);
            _context.Ordenes.Remove(ordenCRM);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrdenCRMExists(int id)
        {
            return _context.Ordenes.Any(e => e.Id == id);
        }
    }
}
